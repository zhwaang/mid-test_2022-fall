# 删除子串题解
----------------
删除子串题目要求：在给定字符串中查找所有特定子串并删除，如果没有找到相应子串，则不作任何操作。要求实现函数：
```c
int delSubStr(char * src, char * sub,char * result);
```
形参说明：
```plain
    src：输入的被操作字符串(函数的“输入”)
    sub：需要查找并删除的特定子字符串(函数的“输入”)
    result：在src字符串中删除所有sub子字符串后的结果(函数的“输出”)
```
返回:
```plain
    删除的子字符串的个数
```
说明:
```plain
    I、子串匹配只考虑最左匹配情况，即只需要从左到右进行字串匹配的情况。
       比如：在字符串"abababab"中，采用最左匹配子串"aba",可以匹配2个"aba"字串。
       如果匹配出从左到右位置2开始的"aba"，则不是最左匹配，且只能匹配出1个"aba"字串。
   II、不用考虑超长字符串的情况。
       示例 : 输入：src = "abcde123abcd123",sub = "123"  输出：result="abcdeabcd"返回：2
```

分析该题目的题意可以看出，主要需要完成的工作有：
1. 计算子串长度len。
2. 定位子串出现的位置。
3. 将用后续字符向前移动len位。
4. 重复1---3，直至句子结束。

## 取子串直接比较法
----------------
可以用通过循环读取src中的字符，从当前位置开始连续取得子串长度的字符保存在一个临时字符串中，然后将该临时字符串与指定的子串进行比较，如果相等，则说明发现了子串，将指针偏移子串长度绕过发现的子串后继续以同样的方式处理后续字符。

在处理中，将扫描到的字符复制到结果中即可，如果发现子串，则同时对其进行计数。

例如在字符串`"abababab"`删除子串`"aba"`的过程如：
```plain
        ---------------------------------------------------
   src->|a|b|a|b|a|b|a|b|'\0'|
        ---------------------------------------------------
               /      /
             /     /
           /    /
         /  /
         v v
        ---------------------------------------------------
result->|b|b|'\0'|
        ---------------------------------------------------
```

其实现细节请参阅`01-pickup-strcmp/main.c`。该方法较为简单，比较直接。

## 内存直接比较法
----------------
通过循环读取src中的字符，从当前位置开始用`memcmp`函数比较子串长度的内存数据是否相等，如果相等，则说明发现了子串，将指针偏移子串长度绕过发现的子串后继续以同样的方式处理后续字符。

在处理中，将扫描到的字符复制到结果中即可，如果发现子串，则同时对其进行计数。

例如在字符串`"abababab"`删除子串`"aba"`的过程如：
```plain
        ---------------------------------------------------
   src->|a|b|a|b|a|b|a|b|'\0'|
        ---------------------------------------------------
               /      /
             /     /
           /    /
         /  /
         v v
        ---------------------------------------------------
result->|b|b|'\0'|
        ---------------------------------------------------
```

其实现细节请参阅`02-locate-memcmp/main.c`。该方法通过直接比较内存数据的方式判断是否存在子串并进行子串删除，比较抽象，但省去了抽取临时字符串的操作。

## strstr函数判定内存移位法
----------------
可以将原问题分解为两个独立的问题：一是判断有多少个子串，二是进行子串删除。

为此，可以直接用`strcpy`函数将src原串复制到目标串result中，然后通过对result串进行处理实现。

可以使用`strstr`函数判断一个子串是否出现在字符串中，通过循环判断，如果发现子串，则将指针偏移子串长度后继续判断，直至结果为NULL，则可以统计出子串出现的个数。

删除子串同样可以使用`strstr`函数实现定位，然后将跳过子串长度后的后续字符串用`memmove`函数复制到当前位置，通过覆盖子串长度的字节删除子串，循环处理后，便可以删除所有子串。

例如在字符串`"abababab"`删除子串`"aba"`的过程如：
```plain
        ---------------------------------------------------
   src->|a|b|a|b|a|b|a|b|'\0'|
        ---------------------------------------------------
                 |
                 v
        ---------------------------------------------------
result->|a|b|a|b|a|b|a|b|'\0'|
        ---------------------------------------------------
         ^   /
         | /
         v
        ---------------------------------------------------
result->|b|a|b|a|b|'\0'|
        ---------------------------------------------------
           ^   /
           | /
           v
        ---------------------------------------------------
result->|b|b|'\0'|
        ---------------------------------------------------
```

其实现细节请参阅`03-memmove-strcpy/main.c`。该方法通过将操作分解为两个步骤，并通过内存移动函数实现了需要的操作，实现较为简洁。

## strstr函数判定内存移位法优化I
----------------
为减少计算量，可以通过提前计算移动字符总数的方式减少长度计算操作，从而提高效率。

其实现细节请参阅`04-memmove-O1/main.c`。

## strstr函数判定内存移位法优化II
----------------
为了进一步减少`memmove`函数的数据移动量，可以通过双指针法，每次仅需要移动必要的数据，从而进一步提高效率。

其实现细节请参阅`05-memmove-O2/main.c`。

## 自定义函数内存复制法
----------------
为减少对`string.h`中相关函数的依赖，可以将`strstr`功能写成一个自字义函数，并将`memmove`函数的操作直接用字节复制的方式进行实现(仍然通过双指针法，每次仅需要移动必要的数据，从而进一步提高效率)。

其实现细节请参阅`06-without-strstr/main.c`。

## 结束语
----------------
由以上分析可以看出，同一个问题可以有多种解决方案，不同方案有不同的优缺点。虽然就一个题目来讲，不需要考虑使用过多的解决方法，以最简单快捷便于理解和编写的方法通过测试即可，但显然多思考、多训练对于C语言程序设计的学习和理解是有极大帮助的。该题目的一个典型特点就是要善于使用相关的函数实现要求的功能，同时，如果不允许使用系统提供的函数，则应该能够写出功能类似的函数以解决问题。在此，本文仅是抛砖引玉，希望各位读者能够给出更为完善的解决方案。

由于个人水平有限，文中难免有不妥之处，甚至会存在错误，也希望各位读者能不吝赐教。

