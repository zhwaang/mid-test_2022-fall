/*--------------------------------------------------------------------------------
* Copyright (c) 2019,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：删除子串的演示代码。
*
* 问题描述：在给定字符串中查找所有特定子串并删除，
*           如果没有找到相应子串，则不作任何操作。要求实现函数：
*           int delSubStr(char * src, char * sub,char * result);
* [形参说明]：
*           src：输入的被操作字符串(函数的“输入”)
*           sub：需要查找并删除的特定子字符串(函数的“输入”)
*           result：在src字符串中删除所有sub子字符串后的结果(函数的“输出”)
* [返回] :
*           删除的子字符串的个数
* 输入：无
* 输出：无
* 输入样例：无
* 输出样例：无
* 提示 :
*     注：I、子串匹配只考虑最左匹配情况，即只需要从左到右进行字串匹配的情况。
*            比如：在字符串"abababab"中，采用最左匹配子串"aba",可以匹配2个"aba"字串。
*            如果匹配出从左到右位置2开始的"aba"，则不是最左匹配，且只能匹配出1个"aba"字串。
*        II、不用考虑超长字符串的情况。
*            示例 : 输入：src = "abcde123abcd123",sub = "123"  输出：result="abcdeabcd"
*            返回：2
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2019年1月16日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>
#include <ctype.h>

/* 宏定义 */
#define BUFLEN 60

/* 函数声明 */
int read_line(char *, int);
int delSubStr(char *, char *, char *);
size_t strcnt(char *, const char *);
char* strremove(char *, const char *);

int main()
{
    char s1[BUFLEN+1] = {0}, s2[BUFLEN+1] = {0}, res[BUFLEN+1] = {0};
    int r;

    /* 测试时也可以使用gets等其它函数 */
    read_line(s1, BUFLEN);
    read_line(s2, BUFLEN);

    /* 执行删除操作 */
    r = delSubStr(s1, s2, res);

    /* 输出结果 */
    printf("%s\n%d\n", res, r);

    return 0;
}

/* 函数定义 */
/*------------------------------------------------------------------
// 名称: int read_line(char *str, int n)
// 功能: 读入指定长度的字符串
// 算法: 逐个读入字符，直到遇到回车符或EOF或是长度大于指定长度。
// 参数:
//       [char *str] --- 结果字符串
//       [int n] ------- 允许读入的最大长度
// 返回: [int]  --- 返回读入字符的个数
// 作者: 耿楠
// 日期: 2019年1月16日
------------------------------------------------------------------ */
int read_line(char *str, int n)
{
    int ch, i = 0;

    /* 跳过前导空格 */
    while(isspace(ch = getchar()))
    {
        ;
    }

    /* 读入数据 */
    while(ch != '\n' && ch != EOF)
    {
        if(i < n)
        {
            str[i++] = ch;
        }
        ch = getchar();
    }

    str[i] = '\0';

    return i;
}

/*------------------------------------------------------------------
// 名称: size_t strcnt(char *str, const char *sub)
// 功能: 统计一个子字符串在指定字符串中出现的次数
// 算法: 子串匹配只考虑最左匹配情况，即只需要从左到右进行字串匹配的情况
//       比如：在字符串"abababab"中，采用最左匹配子串"aba",可以匹配2个"aba"字串。
//       如果匹配出从左到右位置2开始的"aba"，则不是最左匹配，且只能匹配出1个"aba"字串。
// 参数:
//       [char * src] --------- 输入的被操作字符串
//       [const char * sub] --- 需要查找并的子字符串
// 返回: [size_t] --- 一个字符串中包含指定子字符串的个数，若未找到，则返回0
// 作者: 耿楠
// 日期: 2022年12月04日
//-----------------------------------------------------------------*/
size_t strcnt(char *str, const char *sub)
{
    size_t cnt = 0;
    /* 子串长度 */
    size_t len = strlen(sub);
    char *p = str;

    /* 子串不为空 */
    if (len > 0)
    {
        /* 子串计数 */
        while ((p = strstr(p, sub)) != NULL)
        {
            /* 跳过匹配串 */
            p += len;
            cnt++;
        }
    }

    return cnt;
}

/*------------------------------------------------------------------
// 名称: char *strremove(char *str, const char *sub)
// 功能: 按最左匹配，删除一个字符中的指定的所有子串
// 算法: 通过strstr函数实现定位，使用memmove移动后续字符。
// 参数:
//       [char * src] --------- 输入的被操作字符串
//       [const char * sub] --- 需要查找并删除的特定子字符串
// 返回: [char *]  --- 删除后的字符串指针
// 作者: 耿楠
// 日期: 2022年12月04日
//-----------------------------------------------------------------*/
char *strremove(char *str, const char *sub)
{
    char *p, *q, *r;
    size_t len = strlen(sub);

    /* 查找第1次出现位置 */
    r = strstr(str, sub);
    q = r;

    if(*sub && q != NULL)
    {
        /* 指向第1次匹配后的位置 */
        p = r + len;
        r = strstr(p, sub);
        /* 每次仅移动部分内容 */
        while(r != NULL)
        {
            memmove(q, p, r - p);
            q += r - p;
            p = r + len;
            r = strstr(p, sub);
        }
        /* 单独处理第1次匹配 */
        memmove(q, p, strlen(p) + 1);
    }

    return str;
}

/*------------------------------------------------------------------
// 名称: int delSubStr(char * src, char * sub, char * result)
// 功能: 统计一个字符在指定字符串中出现的次数
// 参数:
//       [char * src] --- 输入的被操作字符串
//       [char * sub] --- 需要查找并删除的特定子字符串
//       [char * result] --- 在src字符串中删除所有sub子字符串后的结果
// 返回: [int]  --- 删除的子字符串的个数
// 作者: 耿楠
// 日期: 2019年1月16日
//-----------------------------------------------------------------*/
int delSubStr(char * src, char * sub, char * result)
{
    int cnt = 0;

    /* 复制字符串 */
    strcpy(result, src);

    /* 统计需要删除子串总数 */
    cnt = strcnt(result, sub);

    /* 删除子串 */
    result = strremove(result, sub);

    /* 返回子串个数 */
    return cnt;
}
