#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 最大文件名长度 */
#define MAX_LEN 80

/* 函数原型 */
int read_line(char *s);
void write_excel(const char *in, const char *out);
void print_err_msg(int in_err, int out_err);

void write_xls_from_xls(const char *in, const char *out, int data, int in_err, int out_err);
void write_xls_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err);
void write_xlsx_from_xls(const char *in, const char *out, int data, int in_err, int out_err);
void write_xlsx_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err);

int main()
{
    char in[MAX_LEN];
    char out[MAX_LEN];

    read_line(in);
    read_line(out);

    write_excel(in, out);

    return 0;
}

int read_line(char *s)
{
    int n = 0, ch = 0;

    while((ch = getchar()) != '\n' && ch != EOF && n < MAX_LEN - 1)
        s[n++] = ch;

    s[n]='\0';

    return n;
}

void write_excel(const char *in, const char *out)
{
    int e1, e2, e;
    int in_err, out_err;
    char *p, *q;
    char in_tmp[MAX_LEN];
    char out_tmp[MAX_LEN];

    /* 写入函数指针数组，并初始化 */
    void (*fp[4])(const char *, const char *, int, int, int) = {
         write_xls_from_xls,
         write_xls_from_xlsx,
         write_xlsx_from_xls,
         write_xlsx_from_xlsx
         };

    /* 根据文件后缀名确定输出Excel文件格式 */
    p = strrchr(in, '.');
    q = strrchr(out, '.');

    if(p != NULL)
    {
        if(strcmp(p, ".xls") == 0)
        {
            strcpy(in_tmp, in);
            e1 = 0;
            in_err = 0;
        }
        else if(strcmp(p, ".xlsx") == 0)
        {
            strcpy(in_tmp, in);
            e1 = 1;
            in_err = 0;
        }
        else
        {
            *p = '\0';
            strcpy(in_tmp, in);
            strcat(in_tmp, ".xlsx");
            e1 = 1;
            in_err = 1;
        }
    }
    else
    {
        strcpy(in_tmp, in);
        strcat(in_tmp, ".xlsx");
        e1 = 1;
        in_err = 2;
    }

    if(q != NULL)
    {
        if(strcmp(q, ".xls") == 0)
        {
            strcpy(out_tmp, out);
            e2 = 0;
            out_err = 0;
        }
        else if(strcmp(q, ".xlsx") == 0)
        {
            strcpy(out_tmp, out);
            e2 = 2;
            out_err = 0;
        }
        else
        {
            *q = '\0';
            strcpy(out_tmp, out);
            strcat(out_tmp, ".xlsx");
            e2 = 2;
            out_err = 1;
        }
    }
    else
    {
        strcpy(out_tmp, out);
        strcat(out_tmp, ".xlsx");
        e2 = 2;
        out_err = 2;
    }

    e = e1 + e2;

    /* 用特征值作为函数指针数组下标，调用不同函数 */
    fp[e](in_tmp, out_tmp, 1, in_err, out_err);
}

void print_err_msg(int in_err, int out_err)
{
    switch(in_err)
    {
        case 0:
            printf("Correct suffix of input filename.\n");
            break;
        case 1:
            printf("Error suffix of input filename.\n");
            break;
        case 2:
            printf("Empty suffic of input filename.\n");
            break;
        default:
            break;
    }

    switch(out_err)
    {
        case 0:
            printf("Correct suffix of output filename.\n");
            break;
        case 1:
            printf("Error suffix of output filename.\n");
            break;
        case 2:
            printf("Empty suffic of output filename.\n");
            break;
        default:
            break;
    }
}

void write_xls_from_xls(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLS from %s XLS.\n", out, in);

    print_err_msg(in_err, out_err);
}

void write_xls_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLS from %s XLSX.\n", out, in);

    print_err_msg(in_err, out_err);
}

void write_xlsx_from_xls(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLSX from %s XLS.\n", out, in);

    print_err_msg(in_err, out_err);
}

void write_xlsx_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLSX from %s XLSX.\n", out, in);

    print_err_msg(in_err, out_err);
}
