# 文件后缀名解析
----------------
现需用C语言读入一个Excel电子表格文件，将其内容按需处理后，将结果写入另一Excel文件。并且要求在处理时，不可依赖Excel软件。

Office 2003及之前版的Excel采用的是.xls格式，而2007及之后版采用的是.xlsx格式。这两种格式除了在数据范围上不同外，其文件存储结构也完全不同，需要采用不同的读写方式进行处理。

Excel文件格式是通过文件后缀名(文件名中，最后一个“.”分隔之后的部分)进行区分的，因此，只需提取文件后缀名，并根据后缀名调用不同的处理函数。

在此，根据需要读写的Excel文件格式，设计有如下4个不同数据处理函数：
```c
void write_xls_from_xls(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLS from %s XLS.\n", out, in);
    print_err_msg(in_err, out_err);
}

void write_xls_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLS from %s XLSX.\n", out, in);
    print_err_msg(in_err, out_err);
}

void write_xlsx_from_xls(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLSX from %s XLS.\n", out, in);
    print_err_msg(in_err, out_err);
}

void write_xlsx_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err)
{
    printf("write %s XLSX from %s XLSX.\n", out, in);
    print_err_msg(in_err, out_err);
}
```

其中，`print_err_msg()`函数用于根据读写Excel文件的后缀名输出不同的状态信息：
```c
void print_err_msg(int in_err, int out_err)
{
    switch(in_err)
    {
        case 0:
            printf("Correct suffix of input filename.\n");
            break;
        case 1:
            printf("Error suffix of input filename.\n");
            break;
        case 2:
            printf("Empty suffic of input filename.\n");
            break;
        default:
            break;
    }

    switch(out_err)
    {
        case 0:
            printf("Correct suffix of output filename.\n");
            break;
        case 1:
            printf("Error suffix of output filename.\n");
            break;
        case 2:
            printf("Empty suffic of output filename.\n");
            break;
        default:
            break;
    }
}
```

设其程序主框架为：
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 80

int read_line(char *s);
void write_excel(const char *in, const char *out);
void print_err_msg(int in_err, int out_err);

void write_xls_from_xls(const char *in, const char *out, int data, int in_err, int out_err);
void write_xls_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err);
void write_xlsx_from_xls(const char *in, const char *out, int data, int in_err, int out_err);
void write_xlsx_from_xlsx(const char *in, const char *out, int data, int in_err, int out_err);

int main()
{
    char in[MAX_LEN];
    char out[MAX_LEN];

    read_line(in);
    read_line(out);

    write_excel(in, out);

    return 0;
}

int read_line(char *s)
{
    int n = 0, ch = 0;

    while((ch = getchar()) != '\n' && ch != EOF && n < MAX_LEN - 1)
        s[n++] = ch;

    s[n]='\0';

    return n;
}
```

说明：用户输入的文件名中可以是正确的后缀名.xls或.xlsx，也有可能是错误的后缀名(如.xsl、.xslx等)，甚至没有后缀名。若文件的后缀名错误或无后缀名，则一律按.xlsx后缀名对待，在删除原后缀名，自动添加.xlsx后缀名。

分析该题目的题意可以看出，主要需要完成的工作有：
1. 查找文件名中最后一个标点符号“.”所在的位置。
2. 根据符号“.”位置，提取文件的后缀名。
3. 分析后缀名，确定处理文件的分支。
4. 在不同分支中调用不同处理函数，完成Excel文件的处理。

## 后缀名提取
----------------
由于文件后缀名是文件名中，最后一个“.”后的字符串，因此，只需要在字符串中进行遍历查找，找到最后一个“.”即可实现文件后缀名的提取。这可以使用C语言的`strrchr()`函数，通过在字符串中反向搜索指定字符实现。

```c
/* 根据文件后缀名确定输出Excel文件格式 */
    p = strrchr(in, '.');
    q = strrchr(out, '.');
```

如果指针`p`或`q`不为`NULL`，则表明有后缀名，从指针`p`或`q`开始取到字符串结束即为文件后缀名。如果指针`p`或`q`为`NULL`，则表明无后缀名。

## 直接判断法
----------------
在提取了文件后缀名后，便可以根据文件后缀名，直接采用`strcmp()`函数，通过比较字符串进行多路分支判断，从而实现不同函数的调用。

```c
if(p != NULL)
{
    if(q != NULL)
    {
        if(strcmp(p, ".xls") == 0)
        {
            if(strcmp(q, ".xls") == 0)
            {
                write_xls_from_xls(in, out, 1, 0, 0);
            }
            else if(strcmp(q, ".xlsx") == 0)
            {
                write_xlsx_from_xls(in, out, 1, 0, 0);
            }
            else
            {
                *q = '\0';
                strcpy(out_tmp, out);
                strcat(out_tmp, ".xlsx");
                write_xlsx_from_xls(in, out_tmp, 1, 0, 1);
            }
        }
        else if(strcmp(p, ".xlsx") == 0)
        {
            if(strcmp(q, ".xls") == 0)
            {
                write_xls_from_xlsx(in, out, 1, 0, 0);
            }
            else if(strcmp(q, ".xlsx") == 0)
            {
                write_xlsx_from_xlsx(in, out, 1, 0, 0);
            }
            else
            {
                *q = '\0';
                strcpy(out_tmp, out);
                strcat(out_tmp, ".xlsx");
                write_xlsx_from_xlsx(in, out_tmp, 1, 0, 1);
            }
        }
        else
        {
            *p = '\0';
            strcpy(in_tmp, in);
            strcat(in_tmp, ".xlsx");

            if(strcmp(q, ".xls") == 0)
            {
                write_xls_from_xlsx(in_tmp, out, 1, 1, 0);
            }
            else if(strcmp(q, ".xlsx") == 0)
            {
                write_xlsx_from_xlsx(in_tmp, out, 1, 1, 0);
            }
            else
            {
                *q = '\0';
                strcpy(out_tmp, out);
                strcat(out_tmp, ".xlsx");
                write_xlsx_from_xlsx(in_tmp, out_tmp, 1, 1, 0);
            }
        }
    }
    else
    {
        strcpy(out_tmp, out);
        strcat(out_tmp, ".xlsx");

        if(strcmp(p, ".xls") == 0)
        {
            write_xlsx_from_xls(in, out, 1, 0, 2);
        }
        else if(strcmp(p, ".xlsx") == 0)
        {
            write_xlsx_from_xlsx(in, out, 1, 0, 2);
        }
        else
        {
            *p = '\0';
            strcpy(in_tmp, in);
            strcat(in_tmp, ".xlsx");
            write_xlsx_from_xlsx(in_tmp, out_tmp, 1, 1, 2);
        }
    }
}
else
{
    strcpy(in_tmp, in);
    strcat(in_tmp, ".xlsx");

    if(q != NULL)
    {
        if(strcmp(q, ".xls") == 0)
        {
            write_xls_from_xlsx(in_tmp, out, 1, 2, 0);
        }
        else if(strcmp(q, ".xlsx") == 0)
        {
            write_xlsx_from_xlsx(in_tmp, out, 1, 2, 0);
        }
        else
        {
            *q = '\0';
            strcpy(out_tmp, out);
            strcat(out_tmp, ".xlsx");
            write_xlsx_from_xlsx(in_tmp, out_tmp, 1, 2, 1);
        }
    }
    else
    {
        strcpy(out_tmp, out);
        strcat(out_tmp, ".xlsx");
        write_xlsx_from_xlsx(in_tmp, out_tmp, 1, 2, 2);
    }
}
```

其实现细节请参阅`01-if-else/main.c`。该方法比较直接，但代码结构复杂，编写和维护较为困难。

## 特征值判断法
----------------
显然，利用`if...else if...else`多分支嵌套分支判断，其逻辑结构较为复杂，在编写和维护中容易出错。为此，可以提前先单独对`in`和`out`字符串进行分析处理，将其归一化为只有`.xls`和`.xlsx`两种情况，以简化后续处理过程。

由于`in`和`out`归一化后只有`.xls`和`.xlsx`两种情况，因此可以有4种组合，可以用0、1、2和3共4个不同整数进行区分。为得到这4个整数，可以采用如下方案对`in`的特征值`e1`和`out`的特征值`e2`进行赋值：

|  文件  |后缀名  |特征值|备注  |
| :----: |:----:  |:----:|:----:|
|`in`    |`.xls`  |0     | e1   |
|`in`    |`.xlsx` |1     |      |
|`ou`    |`.xls`  |0     | e2   |
|`out`   |`.xlsx` |2     |      |

特征值赋值操作，可以用如下代码实现：
```c
if(p != NULL)
{
    if(strcmp(p, ".xls") == 0)
    {
        strcpy(in_tmp, in);
        e1 = 0;
        in_err = 0;
    }
    else if(strcmp(p, ".xlsx") == 0)
    {
        strcpy(in_tmp, in);
        e1 = 1;
        in_err = 0;
    }
    else
    {
        *p = '\0';
        strcpy(in_tmp, in);
        strcat(in_tmp, ".xlsx");
        e1 = 1;
        in_err = 1;
    }
}
else
{
    strcpy(in_tmp, in);
    strcat(in_tmp, ".xlsx");
    e1 = 1;
    in_err = 2;
}

if(q != NULL)
{
    if(strcmp(q, ".xls") == 0)
    {
        strcpy(out_tmp, out);
        e2 = 0;
        out_err = 0;
    }
    else if(strcmp(q, ".xlsx") == 0)
    {
        strcpy(out_tmp, out);
        e2 = 2;
        out_err = 0;
    }
    else
    {
        *q = '\0';
        strcpy(out_tmp, out);
        strcat(out_tmp, ".xlsx");
        e2 = 2;
        out_err = 1;
    }
}
else
{
    strcpy(out_tmp, out);
    strcat(out_tmp, ".xlsx");
    e2 = 2;
    out_err = 2;
}
```
注意：在判断分支中，需要给状态码进行正确赋值。

然后就可以构造出如下总特征值`e=e1+e2`计算表：

| e1     |e2      |e=e1+e2|
| :----: | :----: |:----: |
|0       |0       |0      |
|1       |0       |1      |
|0       |2       |2      |
|1       |2       |3      |

其代码为：
```c
e = e1 + e2;
```
在得到特征值`e`后，则可以用其作为条件进行分支操作：
```c
switch(e)
{
case 0:
    write_xls_from_xls(in_tmp, out_tmp, 1, in_err, out_err);
    break;
case 1:
    write_xls_from_xlsx(in_tmp, out_tmp, 1, in_err, out_err);
    break;
case 2:
    write_xlsx_from_xls(in_tmp, out_tmp, 1, in_err, out_err);
    break;
case 3:
    write_xlsx_from_xlsx(in_tmp, out_tmp, 1, in_err, out_err);
    break;
default:
    break;
}
```

说明：特征值e的取值只要能区分4种情况即可，具体取什么值，则可以灵活处理，如：

|  文件  |后缀名  |特征值|备注  |
| :----: |:----:  |:----:|:----:|
|`in`    |`.xls`  |0     | e1   |
|`in`    |`.xlsx` |1     |      |
|`ou`    |`.xls`  |-1    | e2   |
|`out`   |`.xlsx` |1     |      |

此时，得到的`e`的值为：

| e1     |e2      |e=e1+e2|
| :----: | :----: |:----: |
|0       |-1      |-1     |
|1       |-1      |0      |
|0       |1       |1      |
|1       |1       |2      |

只需要调整`case:`语句的取值，就可以实现合理的分支处理。

其实现细节请参阅`02-eigen-switch-case/main.c`。该方法较为抽象，但逻辑清晰，编写和维护简单。

## 特征值判断函数指针法
----------------
注意到`write_*()`4个函数的类型完全相同，则可以将其用一个函数指针数组统一进行管理：
```c
/* 写入函数指针数组，并初始化 */
void (*fp[4])(const char *, const char *, int, int, int) = {
     write_xls_from_xls,
     write_xls_from_xlsx,
     write_xlsx_from_xls,
     write_xlsx_from_xlsx
     };
```
此时，在得到特征值`e`后，便可以将`e`作为数组`fp`的下标索引，实现对函数的调用，从而省略了分支判断操作：
```c
/* 用特征值作为函数指针数组下标，调用不同函数 */
fp[e](in_tmp, out_tmp, 1, in_err, out_err);
```

说明：由于此时的特征值e将被用作数组的下标索引，因此其4个取值必须为0、1、2和3，从而`e1`和`e2`的取值则必须要保证能够得到这4个从0开始的连续整数，除前一种取值方式外，也可以取值为：

|  文件  |后缀名  |特征值|备注  |
| :----: |:----:  |:----:|:----:|
|`in`    |`.xls`  |1     | e1   |
|`in`    |`.xlsx` |2     |      |
|`ou`    |`.xls`  |-1    | e2   |
|`out`   |`.xlsx` |1     |      |

此时，得到的`e`的值为：

| e1     |e2      |e=e1+e2|
| :----: | :----: |:----: |
|1       |-1      |0      |
|2       |-1      |1      |
|1       |1       |2      |
|2       |1       |3      |


其实现细节请参阅`03-eigen-func-pt/main.c`。该方法较为抽象，但逻辑清晰，代码结构简单，编写和维护更为容易。

## 结束语
----------------
通过上述分析，问题编程求解可有多种方案，它们各有优劣，用最直接、快捷、便于理解的方案通过测试无可厚非。然而，尝试不同的思路和方法，持续改进、优化、提升程序性能，是提升编程水平的重要法门。本文仅供抛砖引玉，不当之处请不吝赐教。

