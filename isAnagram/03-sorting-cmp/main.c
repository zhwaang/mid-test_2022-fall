/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：检查单词是否为变位词(相同字母的重新排列)的演示代码。
* 题目：在英语中，如果两个单词中出现的字母相同，
*       并且每个字符出现的次数也相同，
*       那么这两个单词互为变位词(Anagram)。例如：
*       slient与listen、evil与live等互为变位词。
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月06日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 函数原型 */
int is_anagram(const char *, const char *); /* 检查单词是否为变位词(映射数组算法) */
int is_word(const char *);
int cmpchar(const void *, const void*);
void test(char **, char **, size_t n);

int main()
{
    char *p[] = {"listen", "alient" ,"Live" ,"eil", "evil1"};
    char *q[] = {"slient", "listen" , "eviL" ,"live", "live1"};

    test(p, q, 5);

    return 0;
}

void test(char **p, char **q, size_t n)
{
    int i, ret;

    for(i = 0; i < n; i++)
    {
        ret = is_anagram(p[i], q[i]);

        if(ret)
        {
            printf("%s and %s is angram.\n", p[i], q[i]);
        }
        else
        {
            printf("%s and %s is not angram.\n", p[i], q[i]);
        }
    }
}

/*-------------------------------------------------------------------------------------
// 名称: int is_word(const char *s)
// 功能: 检查一个字符串是否为单词
// 参数:
//       [const char *str1] --- 指向字符串的指针
// 返回: [int]  --- 1表示是变位词，0表示不是变位词
// 作者: 耿楠
// 日期: 2018年12月07日
//-------------------------------------------------------------------------------------*/
int is_word(const char *s)
{
    while(*s)
    {
        if(!((*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z')))
        {
            return 0;
        }
        s++;
    }

    return 1;
}

/*-----------------------------------------------------------------------------------------------
// 名称: int cmpchar(const void *alph1, const void *alph2)
// 功能: 比较两个字符的大小
// 参数:
//         [const void *alph1] --- 指向第1个字符的指针
//         [const void *alph2] --- 指向第2个字符的指针
// 返回: [int]  --- 大于返回1，等于返回0，小于返回-1
// 作者: 耿楠
// 日期: 2022年12月06日
// 备注: 该函数是用于qsort快速排序的比较函数
//---------------------------------------------------------------------------------------------*/
int cmpchar(const void *alph1, const void *alph2)
{
    char ch1 = *(const char *)alph1;
    char ch2 = *(const char *)alph2;

    return (ch1 - ch2);
}

/*-------------------------------------------------------------------------------------
// 名称: int is_anagram(const char *str1, const char *str2)
// 功能: 检查两个单词是否为变位词(相同字母的重新排列)
// 算法: 由于组成变位词的字母相同，所以按照字典序排序后，两个字符串是相等的。
//       因此，可以先排序再判断两个字符串是不是相等即可。
// 注意: 如果两个字符串长度不相等，则一定不是变位词。
//       如果字符串中含有非字母字符也一定不是变位词。
// 参数:
//       [const char *str1] --- 指向第1个单词的指针
//       [const char *str2] --- 指向第2个单词的指针
// 返回: [int]  --- 1表示是变位词，0表示不是变位词
// 作者: 耿楠
// 日期: 2022年12月06日
//-------------------------------------------------------------------------------------*/
int is_anagram(const char *str1, const char *str2)
{
    size_t len1 = strlen(str1);
    size_t len2 = strlen(str2);
    int ret;

    /* 字符串指针为空 */
    if(str1 == NULL || str2 == NULL)
    {
        return 0;
    }

    /* 如果长度不等，则一定不是变位词 */
    if(len1 != len2)
    {
        return 0;
    }

    /* 不是单词 */
    if(!is_word(str1) || !is_word(str2))
    {
        return 0;
    }

    /* 复制字符串，以确保不改变原字符串 */
    char *s1 = (char*)malloc((len1 + 1) * sizeof(char));
    if(s1 == NULL)
    {
        return 0;
    }
    strcpy(s1, str1);

    char *s2 = (char*)malloc((len2 + 1) * sizeof(char));
    if(s2 == NULL)
    {
        free(s1);
        return 0;
    }
    strcpy(s2, str2);

    /* 调用qsort对字符串排序 */
    qsort(s1, len1, sizeof(s1[0]), cmpchar);
    qsort(s2, len2, sizeof(s2[0]), cmpchar);

    /* 字符串比较 */
    ret = strcmp(s1, s2);
    /* 释放空间 */
    free(s1);
    free(s2);

    /* 两个字符串相等是变位词，不相等则不是 */
    if(ret == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
