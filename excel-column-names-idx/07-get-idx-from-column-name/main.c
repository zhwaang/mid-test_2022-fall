/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：根据EXCEL表格列字母名称生成列编号。
* 题目：EXCEL的列名称由类似A, B, C, ..., Z, AA, AB, AC, ..., AZ,
*       BA, BB, ..., ZZ, AAA, AAB,...等这样的字符串构成。
*
*       请编写函数将指定的列字母编号转换为从1开始计数的数值编号，要求函数原型为：
*           int get_excel_column_idx(const char *col_name);
*       其输入/输出关系如：
*       Input          Output
*        A              1
*        B              2
*        C              3
*        ...
*        Z              26
*        AA             27
*        AY             51
*        AZ             52
*        CB             80
*        YZ             676
*        ZZ             702
*        AAC            705
*
*       例如对于字母编号CDA，可以转换为：
*       3*26*26 + 4*26 + 1
*       = 26(3*26 + 4) + 1
*       = 26(0*26 + 3*26 + 4) + 1
*       其转换关系为：
*       result = SUM(a[n-i-a]*26^(n-i-1))
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月13日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 列名称最大长度(字符数) */
#define MAXLEN 8

/* 函数原型 */
int get_excel_column_idx(const char *);

int main()
{
    char *idx[] = {"A", "B", "Z", "AY", "AZ", "CB", "YZ",
        "ZY", "ZZ", "AAC", "ZZZ", "ZZZZ", "ZZZZZ"};
    int i;

    for(i = 0; i < sizeof(idx) / sizeof(idx[0]); i++)
    {
        printf("%s---->%d\n", idx[i], get_excel_column_idx(idx[i]));
    }

    return 0;
}

/*--------------------------------------------------------------------
// 名称：int get_excel_column_idx(const char *col_name)
// 功能：根据传入的EXCEL列字母编号生成数值编号
// 参数：
//       [char *col_name] --- EXCEL列字母编号字符串指针
// 返回：[int] --- 数值编号
// 作者：耿楠
// 日期：2022年12月14日
// 参考：https://www.geeksforgeeks.org/find-excel-column-number-column-title/
---------------------------------------------------------------------*/
int get_excel_column_idx(const char *col_name)
{
    int result = 0;
    int i = 0;

    while(col_name[i])
    {
        result *= 26;
        result += col_name[i] - 'A' + 1;
        i++;
    }

    return result;
}
