/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：生成EXCEL表格列名称字符串。
* 题目：EXCEL的列名称由类似A, B, C, ..., Z, AA, AB, AC, ..., AZ,
*       BA, BB, ..., ZZ, AAA, AAB,...等这样的字符串构成。
*
*       假设从0开始计数，则列0对应"A", 列1对应"B"，而列26对应"AA"。
*
*       请编写函数将指定的列编号转换为字符串表示的列名称，要求函数原型为：
*           char* get_excel_column_name(char *col_name, int idx);
*       其输入/输出关系如：
*       Input          Output
*        25             Z
*        50             AY
*        51             AZ
*        79             CB
*        675            YZ
*        701            ZZ
*        704            AAC
*       几个典型值为：
*       Z              25
*       ZZ             701
*       ZZZ            18277
*       ZZZZ           475253
*       ZZZZZ          12356629
*
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月13日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 列名称最大长度(字符数) */
#define MAXLEN 8

/* 函数原型 */
void get_excel_column_name(char **, int);

int main()
{
    char str[MAXLEN] = {0};
    int idx[] = {25, 50, 51, 79, 675, 700, 701, 704, 18277, 475253, 12356629};
    char *p;
    int i;

    for(i = 0; i < sizeof(idx) / sizeof(idx[0]); i++)
    {
        memset(str, 0, MAXLEN);
        p = str + MAXLEN - 1;
        get_excel_column_name(&p, idx[i]);

        printf("%d---->%s\n", idx[i], p);
    }

    return 0;
}

/*--------------------------------------------------------------------
// 名称：void get_excel_column_name(char **p, int idx)
// 功能：根据传入的列编号生成EXCEL的列名称
// 算法：递归算法
// 参数：
//       [char **p] --- 结果字符串尾部的指针的指针(需要有空间可写)
//       [int idx] ---- 从0开始计数的excel列编号
// 返回：[void] --- 无
// 作者：耿楠 李建良
// 日期：2022年12月14日
// 参考：https://codegolf.stackexchange.com/questions/3971/generate-excel-column-name-from-index
---------------------------------------------------------------------*/
void get_excel_column_name(char **p, int idx)
{
    if(idx >= 0)
    {
        --(*p);
        *(*p) = idx % 26 + 'A';
        get_excel_column_name(p, idx / 26 - 1);
    }
}
