#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LEN 20
#define MAX_FRUIT_CLS 20
#define MAX_PATH 256

typedef struct{
    char name[MAX_NAME_LEN];
    float in;
    float out;
}FInfo, *pFInfo;

int loadStock(pFInfo, const char *);
void sortAndSaveStock(pFInfo, int, char *);
void bubbleSort(pFInfo, int);

void catout(FILE *);
void checkout(const char *fname);

int main()
{
    FInfo fruitStock[MAX_FRUIT_CLS];
    char fname[MAX_PATH] = {0};
    int no, n;

    scanf("%d", &no);
    sprintf(fname, "record%d.dic", no);
    n = loadStock(fruitStock, fname);
    sprintf(fname, "sorted_record%d.dic", no);
    if ( n > 0)  sortAndSaveStock(fruitStock, n, fname);
    checkout(fname);
    return 0;
}

void catout(FILE *fp)
{
    char line[MAX_PATH]={0};

    rewind(fp);
    while( ! feof(fp))
    {
        if (fgets(line, MAX_PATH, fp) == NULL)
            break;
        printf("%s",line);
        memset(line, 0, MAX_PATH);
    }
}

void checkout(const char *fname)
{
    FILE *fp;
    int len = 0;

    fp = fopen(fname,"r");
    if (!fp) return;
    fseek(fp, 0L, SEEK_END);
    len = (int) ftell(fp);

    printf("File Len = %d.\n", len);
    catout(fp);
    fclose(fp);
}

/* 读取库存数据 */
int loadStock(pFInfo stocks, const char *fname)
{
    FILE *fp;
    int i, n;

    /* 以读方式打开文本文件 */
    fp = fopen(fname, "r");
    if(fp == NULL)
    {
        return -1;
    }

    i = 0;
    while(!feof(fp))
    {
        /* 从文件当前行读入数据 */
        n = fscanf(fp, "%s%f%f", stocks->name, &stocks->in, &stocks->out);

        /* 读取字段数正常 */
        if (n >= 3)
        {
            stocks++;
            i++;
        }
        else
        {
            break;
        }
    }

    /* 关闭文件 */
    fclose(fp);

    return i;
}

/* 冒泡排序 */
void bubbleSort(pFInfo stocks, int n)
{
    FInfo tmp;
    int i, j;
    float st1, st2;

    /* 外层循环 */
    for(i = 0; i < n - 1; i++ )
    {
        /* 内层循环 */
        for(j = 0; j < n - 1 - i; j++)
        {
            st1 = stocks[j].in - stocks[j].out;
            st2 = stocks[j + 1].in - stocks[j + 1].out;
            if(st1 < st2)
            {
                tmp = stocks[j];
                stocks[j] = stocks[j + 1];
                stocks[j + 1] = tmp;
            }
        }
    }
}

/* 库存排序及保存 */
void sortAndSaveStock(pFInfo stocks, int n, char *fname)
{
    FILE *fp;
    int i;

    /* 以写的方式打开文本文件 */
    fp = fopen(fname, "w");
    if(fp == NULL)
    {
        return;
    }

    /* 对库存排序 */
    bubbleSort(stocks, n);

    /* 将结果写入文本文件 */
    for(i = 0; i < n - 1; i++)
    {
        /* 将一行数据写入文件 */
        fprintf(fp, "%s %0.1f %0.1f\n", stocks[i].name, stocks[i].in, stocks[i].out);
    }
    /* 最后一行不写入'\n' */
    fprintf(fp, "%s %0.1f %0.1f", stocks[i].name, stocks[i].in, stocks[i].out);

    /* 关闭文件 */
    fclose(fp);
}
