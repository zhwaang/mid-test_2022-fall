#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 128

/* 函数原型 */
int CheckLuhn ( const char * cardId, int sum[2] );
int ReadLine(char *s);

int main()
{
    int s[2]={0}, r;
    char id[MAX_LEN];

    while (ReadLine(id) > 0) {
        r = CheckLuhn(id, s);
        printf("%s,R=%d\t%d\t%d\n",id, r, s[0], s[1]);
    }
    return 0;
}

/* 字符串读取 */
int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

/* Luhn模10校验 */
int CheckLuhn( const char * cardId, int sum[2])
{
    int len, tmp;
    int i;

    if ( cardId != NULL )
    {
        len = strlen ( cardId );
        /* 累加器清0(非常重要) */
        sum[0] = sum[1] = 0;

        /* 反向遍历 */
        for ( i = len - 1; i >= 0; i-- )
        {
            if ( ( len - i ) % 2 == 1 ) /* 奇数位 */
            {
                sum[0] += cardId[i] - '0';
            }
            else                        /* 偶数位 */
            {
                tmp = ( cardId[i] - '0' ) * 2;
                sum[1] += tmp >= 10 ? tmp - 9 : tmp;
            }
        }
    }

    /* 校验结果 */
    if ( ( sum[0] + sum[1] ) % 10 ) return 1;
    return 0;
}
