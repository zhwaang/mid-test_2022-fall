#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 128

/* 函数原型 */
int CheckLuhn ( const char * cardId, int sum[2] );
int ReadLine(char *s);

int main()
{
    int s[2]={0}, r;
    char id[MAX_LEN];

    while (ReadLine(id) > 0) {
        r = CheckLuhn(id, s);
        printf("%s,R=%d\t%d\t%d\n", id, r, s[0], s[1]);
    }
    return 0;
}

/* 字符串读取 */
int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

/* Luhn模10校验 */
int CheckLuhn(const char * cardId, int sum[2])
{
    int i, k;
    int len = strlen(cardId);
    i = len;

    /* 累加器清0(非常重要) */
    sum[0] = 0;
    sum[1] = 0;

    /* 反向遍历字符串 */
    while (i--)
    {
        /* 数字字符转换成整数 */
        k = cardId[i] & 0x0F;
        /* 反向计数偶数位乘2(假设编译器真返回1,假返回0) */
        k <<= !((len - i) % 2);
        /* 乘2结果为两位数时减9(假设编译器真返回1,假返回0) */
        k -= 9 * (k > 9);

        /* 根据位置奇偶性累加 */
        if((len - i) % 2)
        {
            sum[0] += k;
        }
        else
        {
            sum[1] += k;
        }
    }

    /* 校验并返回 */
    return ((sum[0] + sum[1]) % 10) ? 1 : 0;
}

