#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 128

/* 函数原型 */
int CheckLuhn ( const char * cardId, int sum[2] );
int ReadLine(char *s);

int main()
{
    int s[2]={0}, r;
    char id[MAX_LEN];

    while (ReadLine(id) > 0) {
        r = CheckLuhn(id, s);
        printf("%s,R=%d\t%d\t%d\n", id, r, s[0], s[1]);
    }
    return 0;
}

/* 字符串读取 */
int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

/* Luhn模10校验 */
int CheckLuhn(const char * cardId, int sum[2])
{
    int i, k, num, len;
    int even_flag;
    /* 偶数位计算结果查找表 */
    int even_lut[] = {0, 2, 4, 6, 8, 1, 3, 5, 7, 9};

    len = strlen(cardId);

    even_flag = (len & 1) ? 0 : 1;
    k = 0;

    /* 累加器清0(非常重要) */
    sum[0] = 0;
    sum[1] = 0;

    /* 正向遍历 */
    for(i = 0; i < len; i++)
    {
        /* 提取数字 */
        num = cardId[i] & 0x0F;
        if(!even_flag) /* 奇数位 */
        {
            sum[0] += num;
        }
        else           /* 偶数位 */
        {
            sum[1] += even_lut[num];
        }
        /* 奇偶位状态切换 */
        even_flag = !even_flag;
    }

    /* 计算校验结果并返回 */
    k = sum[0] + sum[1];

    return (k % 10) ? 1 : 0;
}

