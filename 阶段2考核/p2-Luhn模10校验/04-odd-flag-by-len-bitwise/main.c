#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 128

/* 函数原型 */
int CheckLuhn ( const char * cardId, int sum[2] );
int ReadLine(char *s);

int main()
{
    int s[2]={0}, r;
    char id[MAX_LEN];

    while (ReadLine(id) > 0) {
        r = CheckLuhn(id, s);
        printf("%s,R=%d\t%d\t%d\n", id, r, s[0], s[1]);
    }
    return 0;
}

/* 字符串读取 */
int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

/* Luhn模10校验 */
int CheckLuhn( const char * cardId, int sum[2])
{
    int i, k, num, len;
    int odd_flag;

    len = strlen(cardId);

    odd_flag = (len & 1) ? 1 : 0;
    k = 0;

    /* 累加器清0(非常重要) */
    sum[0] = 0;
    sum[1] = 0;

    /* 正向遍历 */
    for(i = 0; i < len; i++)
    {
        /* 提取数字 */
        num = cardId[i] & 0x0F;
        if(odd_flag) /* 奇数位 */
        {
            sum[0] += num;
            odd_flag = 0; /* 切换为偶数位状态 */
        }
        else         /* 偶数位 */
        {
            num <<= 1; /* 左移1位相当于乘2 */
            if (num > 9) num -= 9;
            sum[1] += num;
            odd_flag = 1; /* 切换为奇数位状态 */
        }
    }

    /* 计算并返回 */
    k = sum[0] + sum[1];

    return (k % 10) ? 1 : 0;
}

