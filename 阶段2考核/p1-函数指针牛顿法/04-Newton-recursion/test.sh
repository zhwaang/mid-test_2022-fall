#!/bin/bash

tmp="test.out"

echo "testing..."

for file in `ls ../testData/*.in`
  do
    filename="${file##*/}"
    ./test < $file > $tmp
    # 输出两个文件的比较结果到黑洞
    diff $tmp "${file%.in}.out" > /dev/null
    if [ $? != 0 ]; then
      echo "$file case failed!"
      diff $tmp "${file%.in}.out" > "${filename%.in}_test.log"
    fi
    rm $tmp
  done

echo "done"
