#include <stdio.h>
#include <string.h>

#define MAX_LEN 10

/* 函数原型 */
char *strrev(char *);
int Judge(char *, int);
int ReadLine(char *s);

int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

int main()
{
    char str[MAX_LEN];
    int n;
    while(ReadLine(str) > 0){
        scanf("%d", &n);
        getchar();
        printf("%d\n", Judge(str, n));
    }
    return 0;
}

/* 字符串逆序 */
char *strrev(char *str)
{
      char *p1, *p2;

      /* 字符串指针为NULL或字符串为空 */
      if (! str || ! *str)
      {
          return str;
      }

      /* 双向遍历交换 */
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }

      return str;
}

/* 判断是否限行 */
int Judge(char *plate_num, int day)
{
    int i, k, m = -1;
    char *s = plate_num;
    int len = strlen(plate_num);

    /* 车牌号合法字符查找表 */
    char plate_num_lut[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* 非法车牌号和星期数 */
    if((len != 5 && len != 6) || day < 1 || day > 7)
    {
        return -1;
    }

    /* 车牌号中含有非法字符(查找表实现) */
    for(i = 0; i < len; i++)
    {
        if(!strchr(plate_num_lut, plate_num[i]))
        {
            return -1;
        }
    }

    /* 6位新能源车牌号和周六周日不限行 */
    if(len == 6 || day >= 6)
    {
        return 0;
    }

    /* 车牌号字符串逆序 */
    strrev(plate_num);

    k = -1;
    /* 读取尾号 */
    while(sscanf(s, "%1d", &k) != 1 && *s != '\0')
    {
        s++;
    }

    /* 未读到尾号 */
    if(k == -1)
    {
        return -1;
    }

    /* 根据尾号确定限行星期数 */
    switch(k)
    {
      case 1: case 6:
            m = 1;
            break;
      case 2: case 7:
            m = 2;
            break;
      case 3: case 8:
            m = 3;
            break;
      case 4: case 9:
            m = 4;
            break;
      case 5: case 0:
            m = 5;
            break;
      default:
            m = -1;
            break;
    }

    /* 限行判断 */
    if(m == day)
    {
        return 1;
    }

    return 0;
}

