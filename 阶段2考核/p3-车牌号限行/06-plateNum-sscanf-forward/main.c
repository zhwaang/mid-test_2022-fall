#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 10

/* 函数原型 */
int isValidPlateNum(const char *);
int Judge(char *, int);
int ReadLine(char *s);

int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

int main()
{
    char str[MAX_LEN];
    int n;
    while(ReadLine(str) > 0){
        scanf("%d", &n);
        getchar();
        printf("%d\n", Judge(str, n));
    }

    return 0;
}

/* 判断车牌号是否有效 */
int isValidPlateNum(const char *plate_num)
{
    const char *s = plate_num;
    /* 查找表 */
    char digit_lut[] = "0123456789";
    char plate_num_lut[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    int flag = 0;
    int len = strlen(plate_num);

    /* 长度不符合要求 */
    if(len != 5 && len != 6)
    {
        return 0;
    }

    /* 字符判断 */
    while(*s)
    {
        /* 含非法字符 */
        if(!strchr(plate_num_lut, *s))
        {
            return 0;
        }

        /* 至少需要有一个数字字符 */
        if(strchr(digit_lut, *s))
        {
            flag = 1;
        }
        s++;
    }

    return flag;
}

/* 判断是否限行 */
int Judge(char *plate_num, int day)
{
    int k;
    int len = strlen(plate_num);

    /* 车牌号或星期数非法 */
    if(!isValidPlateNum(plate_num) || day < 1 || day > 7)
    {
        return -1;
    }

    /* 新能源车(6位)和周六周日不限行 */
    if(len == 6 || day >= 6)
    {
        return 0;
    }

    /* 遍历合法车牌号字符串提取尾号 */
    while(*plate_num)
    {
        /* 读取1位数字 */
        sscanf(plate_num, "%1d", &k);
        plate_num++;
    }

    /* 判断并返回结果 */
    return !((k % 5) ^ (day % 5));
}
