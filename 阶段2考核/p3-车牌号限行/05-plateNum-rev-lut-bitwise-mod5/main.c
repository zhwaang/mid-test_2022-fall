#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 10

/* 函数原型 */
char *strrev(char *);
int isValidPlateNum(const char *);
int Judge(char *, int);
int ReadLine(char *s);

int ReadLine(char *s)
{
    int n = 0, ch = 0;
    while((ch = getchar()) != '\n' && ch != EOF && n <= MAX_LEN - 1)
        s[n++] = ch;
    s[n]='\0';
    return n;
}

int main()
{
    char str[MAX_LEN];
    int n;
    while(ReadLine(str) > 0){
        scanf("%d", &n);
        getchar();
        printf("%d\n", Judge(str, n));
    }

    return 0;
}

/* 字符串逆序 */
char *strrev(char *str)
{
      char *p1, *p2;

      /* 字符串指针为NULL或字符串为空 */
      if (! str || ! *str)
      {
          return str;
      }

      /* 双向遍历交换 */
      for (p1 = str, p2 = str + strlen(str) - 1; p2 > p1; ++p1, --p2)
      {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
      }

      return str;
}

/* 判断车牌号是否有效 */
int isValidPlateNum(const char *plate_num)
{
    const char *s = plate_num;
    /* 查找表 */
    char digit_lut[] = "0123456789";
    char plate_num_lut[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    int flag = 0;
    int len = strlen(plate_num);

    /* 长度不符合要求 */
    if(len != 5 && len != 6)
    {
        return 0;
    }

    /* 字符判断 */
    while(*s)
    {
        /* 含非法字符 */
        if(!strchr(plate_num_lut, *s))
        {
            return 0;
        }

        /* 至少需要有一个数字字符 */
        if(strchr(digit_lut, *s))
        {
            flag = 1;
        }
        s++;
    }

    return flag;
}

/* 判断是否限行 */
int Judge(char *plate_num, int day)
{
    int k;
    char *s;
    int len = strlen(plate_num);

    /* 车牌号或星期数非法 */
    if(!isValidPlateNum(plate_num) || day < 1 || day > 7)
    {
        return -1;
    }

    /* 新能源车(6位)和周六周日不限行 */
    if(len == 6 || day >= 6)
    {
        return 0;
    }

    /* 字符串逆序 */
    strrev(plate_num);
    s = plate_num;

    /* 遍历逆序后的车牌号字符串 */
    while(*s)
    {
        /* 是数字字符(位运算实现) */
        /* 数字字符与大写字母之间存在第7位的0和1的差别 */
        if (!(*s & 0x40))
        {
            /* 转换为整型数字 */
            /* 仅保留数字字符的低4位即可 */
            k = *s & 0x0F;
            break;
        }
        s++;
    }

    /* 判断并返回结果 */
    return !((k % 5) ^ (day % 5));
}
