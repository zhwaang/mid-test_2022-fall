# 选择题答题样本生成
----------------
为设计调查问卷投票结果分析算法，现需要生成单选和多选题目投票结果模拟数据。现要求设计一函数根据选项总数、最少选择的选项数和最多选择的选项数返回一个选择结果整数。该整数的二进制位的对应位置代表选项编号，1表示选中，0表示未选中。

函数原型要求如下：
```c
    int get_selected(int tasks, int low, int high);
```

分析该题目的题意可以看出，主要需要完成的工作有：
1. 确定整数长度，也就是二进制位数。这可以根据最多选项数确定。
2. 设计位段结构以方便处理整数的各个位。
3. 确定最大整数值top。
4. 生成[1, top]之间的一个随机数，并统计其二进制位中1的个数。
5. 判断二进制位1的总数是否处于[low, high]之间。
6. 如果不满足要求，则丢弃该随机数，重新生成，直到满足条件。
7. 逐个取出各位，输出1或0表示选中或未选中该选项。

## 定义位段
----------------
假设最多选项为8个选项，则用一个字节就可以表示所有选项的选择结果，为些，可定义如下位段结构：
```c
typedef struct
{
    unsigned short a0: 1;
    unsigned short a1: 1;
    unsigned short a2: 1;
    unsigned short a3: 1;
    unsigned short a4: 1;
    unsigned short a5: 1;
    unsigned short a6: 1;
    unsigned short a7: 1;
} TASKSDATA;
```
为便于统计二进制中1的个数，可以定义如下带参数的宏实现这一操作：
```c
/* 利用位段统计一个字节中1出现的次数 */
#define  sumbites(b) (b.a0+b.a1+b.a2+b.a3+b.a4+b.a5+b.a6+b.a7)
```

## 生成随机整数
----------------
C语言提供了**伪随机数**生成函数`rand()`，为了生成“真”随机数，则需要在每次运算程序时置随机种子，可以取得当前时间作为随机种子，如：
```c
/* 置随机种子 */
srand((unsigned)time(NULL));
```
当然，为了生成指定范围的整数，可以参考Eric S. Roberts著“程序设计抽象思想--C语言描述”3.2节给出的方法生成随机整数：
```c
/* 生成[low, high]之间的随机整数 */
int get_rand_int(int low, int high)
{
    int k;
    double d;

    d = (double) rand() / ((double) RAND_MAX + 1);
    k = (int) (d * (high - low + 1));
    return (low + k);
}
```

## 确定随机整数上限
----------------
随机整数上限根据选项`tasks`的值确定，为此，可以利用位运算构造该上限：
```c
/* 随机数上限(选项数) */
top = ((unsigned char) ~0) >> (sizeof(unsigned char) * 8 - tasks);
```
也就是将0各位取把得到各二进制位全为1的一个整数，然后根据传入的选项总数，右移位后得到该随机整数上限。

## 循环生成结果整数
----------------
利用`sumbites`宏统计二进制位中1的个数，结合要求的最少和最多选项选择数作为循环控制条件便可生成需要的整数：
```c
    /* 随机数 */
    task = get_rand_int(1, top);
    /* 内存复制 */
    memcpy(&p, &task, sizeof(unsigned char));
    /* 统计1的个数 */
    cnt_ones = sumbites(p);

    /* 选择项数范围判定 */
    while(!(cnt_ones >= low && cnt_ones <= high))
    {
        task = get_rand_int(1, top);
        memcpy(&p, &task, sizeof(unsigned char));
        cnt_ones = sumbites(p);
    }
```

## 结果输出
----------------
通过移位和取位操作(|或运算)，便可以得到输出结果：
```c
/* 输出选择结果 */
void print_selected(int tasks, int sel)
{
    int i;

    for(i = 0; i < tasks - 1; i++)
    {
        printf("item%d\t", i + 1);
    }
    printf("item%d\n", i + 1);

    for(i = 0; i < tasks - 1; i++)
    {
        printf("%d\t", (sel >> i) & 1);
    }
    printf("%d\n", (sel >> i) & 1);
}
```

其实现细节请参阅`main.c`。

## 结束语
----------------
由以上分析可以看出，使用位运算可以以另一种思路解决问题。在遇到问题时，要多思考，将学过的知识综合应用，一方面是解决问题，另一方面也是对学过的知识进行巩固和提升。

由于个人水平有限，文中难免有不妥之处，甚至会存在错误，也希望各位读者能不吝赐教。
