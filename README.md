# midTest_2022Fall
------------------

## 介绍
------------------
西北农林科技大学信息工程学院计算机类C语言程序设计考试及习题解析与参考代码。

题目及要求，请在校园网内通过NWAFU-OJ平台查阅。


## 贡献
------------------
由于受能力所限，无保证代码设计的准确性和合理性。如果您有任何改进意见或者功能需求，欢迎提交 [issue](https://gitee.com/zhwaang/mid-test_2022-fall/issues) 或 [pull request](https://gitee.com/zhwaang/mid-test_2022-fall/pulls)。

作者：耿楠，李建良，杨会君，牛当当，王振华
