/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：手机九宫格按键次数统计
* 题目：手机九宫格的键盘如下所示，要按数字键多下才能按出英文字母。例如，按9一个会出w
*       按9两下会出x；按0一下会出一个空格
*       1       2abc    3def
*       4ghi    5jkl    6mno
*       7pqrs   8tuv    9wxyz
*       *       0       #
*       现需要读取一行只包含英文字母和空格的句子(不超过200个字符并以#号结束)，统计其
*       在手机上需要按下的键盘次数，例如：
*       i have a dream
*       则需要按下23次键。
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月14日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 201

/* 函数原型 */
int count_cellphone_key_pressed(const char *);

int main()
{
    char *str = "i have a dream"; /* 23次 */
    /* char *str = "abcdef"; #<{(| 12次 |)}># */
    /* char *str = "ssss"; #<{(| 16次 |)}># */

    printf("%d\n", count_cellphone_key_pressed(str));

    return 0;
}

/*--------------------------------------------------------------------
// 名称：int count_cellphone_key_pressed(const char *str)
// 功能：读入文本文件中指定的行
// 算法：查找表法统计计数
// 参数：
//       [char *str] --- 需要输入的字符串指针
// 返回：[char*] --- 读入的字符串
// 作者：耿楠
// 日期：2022年12月14日
---------------------------------------------------------------------*/
int count_cellphone_key_pressed(const char *s)
{
    int sum = 0;
    /* 字母按键次数查找表 */
    char *press1 = "adgjmptw ";
    char *press2 = "behknqux";
    char *press3 = "cfilorvy";
    char *press4 = "sz";

    while(*s)
    {
        if(strchr(press1, *s))
        {
            sum += 1;
        }
        else if(strchr(press2, *s))
        {
            sum += 2;
        }
        else if(strchr(press3, *s))
        {
            sum += 3;
        }
        else if(strchr(press4, *s))
        {
            sum += 4;
        }

        s++;
    }

    return sum;
}
