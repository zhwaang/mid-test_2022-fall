#include <stdio.h>
#include <stdlib.h>

#define N 100
#define DELTA 1e-10

float calPrice(float price);


int main(void)
{
    int n, i;
    
    float items[N];

    float *p = items;

    float price = 0.0f;

    n = 0;
    while (scanf("%f", p++) != EOF)
    {
        n ++;
    }

    float newPrice = 0.0f;
    for (i = 0; i < n; i ++)
    {
        newPrice = calPrice(items[i]);
        printf("%.2f\t\t%.2f\n", items[i], newPrice);
    }

    return 0;
}

float calPrice(float price)
{
    float newPrice;

    if ((price - 100.0f) < DELTA)
        newPrice = price;
    else if ((price - 200.0f) <= DELTA)
        newPrice = price * 0.95f;
    else
        newPrice = price * 0.92f;
    return newPrice;
}
