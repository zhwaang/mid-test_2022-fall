#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 20

void LeastSquare(double x[], double y[], int num, double *w, double *b);
void input(double fv[], int n);

int main()
{
	double x[N];
	double y[N];
	int n;
	scanf("%d", &n);
	input(x, n);
	input(y, n);
	double w = 0;
	double b = 0;
	LeastSquare(x, y, n, &w, &b);
	printf("%.2f %.2f\n", w, b);
	return 0;
}

/* 以上代码学生可看到，但不能修改 */
void input(double fv[], int n)
{
	int i;
	for (i = 0; i < n; i++) {
		scanf("%lf", fv + i);
	}
}

void LeastSquare(double x[], double y[], int num, double *w, double *b)
{
	double mul_xy = 0.0;	/* XY 的积 */
	double sum_x2 = 0.0;	/* X 的平方和 */
	double sum_y = 0.0;	/* Y 的和 */
	double avg_y = 0.0;	/* Y 的平均值 */
	double sum_x = 0.0;	/* X 的和 */
	double avg_x = 0.0;	/* X 的平均值 */

	for (int i = 0; i < num; ++i) {
		/* 求 XY 的积 */
		mul_xy += x[i] * y[i];
		/* 求 X 的平方和 */
		sum_x2 += x[i] * x[i];
		/* 求 X 的和 */
		sum_x += x[i];
		/* 求 Y 的和 */
		sum_y += y[i];
	}

	/* 求 x 与 y 的平均值 */
	avg_x = sum_x / num;
	avg_y = sum_y / num;

	*w = (mul_xy - avg_x * sum_y) / (sum_x2 - pow(avg_x,2) * num);
	*b = (avg_y - *w * avg_x);
	if (fabs(*w) <= 0.001)
		*w = 0.0;
	if (fabs(*b) <= 0.001)
		*b = 0.0;
}
