#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 20

void LeastSquare(double x[], double y[], int num, double *w, double *b);
void input(double fv[], int n);

int main()
{
    double x[N], y[N];
    int n;
    double w = 0, b = 0;

    scanf("%d", &n);
    input(x, n);
    input(y, n);
    LeastSquare(x, y, n, &w, &b);
    printf("%.2f %.2f\n", w, b);

    return 0;
}

void input(double fv[], int n)
{
    int i;
    for (i = 0; i < n; i ++)
    {
        scanf("%lf", &fv[i]);
    }
}

void LeastSquare(double x[], double y[], int num, double *w, double *b)
{
    double xbar = 0.0f;
    double wtemp, btemp;

    double d1, d2;
    d1 = d2 = 0.0;
    int i, j, k;

    for (i = 0; i < num; i ++)
    {
        xbar = xbar + x[i];
    }

    xbar = xbar / num;

    for (j = 0; j < num; j ++)
    {
        d1 = d1 + y[j] * (x[j] - xbar);
        d2 = d2 + x[j] * x[j];
    }
    d2 = d2 - num * xbar * xbar;
    wtemp = d1 / d2;

    d1 = 0.0;
    for (k = 0; k < num; k ++)
    {
        d1 = d1 + y[k] - wtemp * x[k];
    }

    btemp = d1 / num;

    *w = wtemp;
    *b = btemp;

    if (fabs(*w) <= 0.001)
        *w = 0.00;
    if (fabs(*b) <= 0.001)
        *b = 0.00;
}

