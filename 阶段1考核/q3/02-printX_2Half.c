#include <stdio.h>

void topHalf(char ch, int n);
void botHalf(char ch, int n);
char outChar(int n);


int main(void)
{
    int n;
    char ch;

    scanf("%d %c", &n, &ch);

    if (n >= 13 || n <= 0 || ch > 'Z' || ch < 'A')
    {
        printf("Input Data Error.\n");

        return 1;
    }
    topHalf(ch, n);
    botHalf(ch, n);

    return 0;
}

char outChar(int n)
{
    if (n > 'Z')
        return n - 26;
    else if (n < 'A')
        return n + 26;
    else
        return n;
}

void topHalf(char ch, int n)
{
    int i, j;
    int m = 2 * n - 1;

    for (i = 0; i < n; i++, printf("\n"))
    {
        for (j = 0; j < m; j ++)
        {
            if (j == i || j == m - 1 - i)
                printf("%c", outChar(ch - (n - 1) + i));
            else
                printf(" ");
        }
    }
}

void botHalf(char ch, int n)
{
    int i, j;
    int m = 2 * n - 1;

    for (i = 1; i < n; i++, printf("\n"))
    {
        for (j = 0; j < m; j ++)
        {
            if (j == n + (i - 1) || j == n - (i + 1))
                printf("%c", outChar(ch + i));
            else
                printf(" ");
        }
    }
}
