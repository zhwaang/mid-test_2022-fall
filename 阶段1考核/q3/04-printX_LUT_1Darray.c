#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int GetStartOffs(char, int);
char * Create1DCharArray(int);
void SetDiagBackDiag(char *, int, int);
void OutputChar(char *, int);
void PrintX(char ch, int n);

int main()
{
    int n;
    char ch;

    scanf("%d %c", &n, &ch);
    if (n >= 13 || n <= 0 || ch > 'Z' || ch < 'A')
    {
        printf("Input Data Error.\n");
        return 1;
    }
    PrintX(ch, n);

    return 0;
}

/* 获取起始字符在字符表中的下标索引 */
int GetStartOffs(char ch, int n)
{
    int k;

    /* 当前字符在字母表中的下标索引 */
    int idx = ch - 'A';

    /* 计算起始字母索引偏移 */
    idx = idx - (n - 1);

    /* 判断有多少个26 */
    k = abs(idx) / 26;

    /* 如果偏移后的idx为负值，则通过加(k+1)个26按环形结构偏移到正确位置 */
    /* 如果偏移后的idx为正值，则加(k+1)个26不影响结果 */
    idx = (idx + (k + 1) * 26) % 26;

    return idx;
}

/* 申请字符串数组空间，并初始化 */
char * Create1DCharArray(int len)
{
    char *s = NULL;

    /* 申请内存 */
    s = (char*)malloc(len * sizeof(char));
    if(NULL == s)
    {
        exit(1);
    }
    memset(s, ' ', len * sizeof(char));

    return s;
}

/* 设置主副对角线元素 */
void SetDiagBackDiag(char *s, int idx, int m)
{
    int i;

    /* 查找表 */
    char *ch_sets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* 主副对角线赋值 */
    for (i = 0; i < m; i++)
    {
        *(s + i * m + i) = ch_sets[(idx + i) % 26];
        *(s + i * m + (m - 1 - i)) = ch_sets[(idx + i) % 26];
    }
}

/* 输出结果 */
void OutputChar(char *s, int m)
{
    int i, j;

    /* 循环输出每个字符 */
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < m; j++)
        {
            printf("%c", s[i * m + j]);
        }
        printf("\n");
    }
}

/* 这是一个泛化的函数，可以处理任意长度的n */
void PrintX(char ch, int n)
{
    char *s = NULL;
    size_t m = (2 * n - 1);
    size_t len = m * m;

    /* 起始字符在查找表中的位置 */
    int idx = GetStartOffs(ch, n);

    /* 创建记录结果的字符串数组 */
    s = Create1DCharArray(len);

    SetDiagBackDiag(s, idx, m);

    OutputChar(s, m);

    free(s);
}
