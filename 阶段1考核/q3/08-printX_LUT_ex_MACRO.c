#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 字符集宏 */
/* #define CH_SETS ABCDEFGHIJKLMNOPQRSTUVWXYZ */
#define CH_SETS ABCx.@c*>

/* #宏运算符将宏参数转换为字面串 */
#define MAKE_STR(arg) #arg

/* 由于MAKE_STR中有#宏运算符，因此将不展开#紧邻的内层宏参数
// 为构造字符集字面串，则再封装一个无#宏运算符的宏。
// 以便先展开内层宏参数，再展开外层宏函数*/
#define GET_CH_SETS_STR(arg) MAKE_STR(arg)
/* 利用字面串拼接用字符集宏构造scanf的扫描列表 */
#define GET_SCAN_SETS(arg) "%*[^"MAKE_STR(arg)"]"

int GetStartOffs(char, int, const char*);
void PrintX(char ch, int n, const char*);

int main()
{
    int n;
    char ch;
    /* 使用宏构造字面串 */
    char *ch_sets = GET_CH_SETS_STR(CH_SETS);

    /* 利用扫描列表清除前导非数字字符 */
    scanf("%*[^0-9]");
    if(scanf("%d", &n) != 1)
    {
        n = -1;
    }
    /* 利用宏构造扫描列表清除非字符集中的字符 */
    scanf(GET_SCAN_SETS(CH_SETS));
    if(scanf("%c", &ch) != 1)
    {
        ch = 0;
    }

    /* 根据字符集长度取模，所以不受长度限制 */
    if (n <= 0 || !strchr(ch_sets, ch))
    {
        printf("Input Data Error.\n");
        return 1;
    }

    PrintX(ch, n, ch_sets);

    return 0;
}

/* 获取起始字符在字符表中的下标索引 */
int GetStartOffs(char ch, int n, const char *ch_sets)
{
    int idx, k;
    char *ch_cur;
    int mod = strlen(ch_sets);

    /* 找到当前字符在字符集中的地址 */
    ch_cur = strchr(ch_sets, ch);
    /* 计算下标索引 */
    idx = ch_cur - ch_sets;

    /* 偏移到起始字符 */
    idx = idx - (n  - 1);

    /* 判断有多少个len */
    k = abs(idx) / mod;

    /* 如果偏移后的idx为负值，则通过加(k+1)个mod按环形结构偏移到正确位置 */
    /* 如果偏移后的idx为正值，则加(k+1)个mod不影响结果 */
    idx = (idx + (k + 1) * mod) % mod;

    return idx;
}

void PrintX(char ch, int n, const char *ch_sets)
{
    int i, j;
    int m = 2 * n - 1;
    int mod = strlen(ch_sets);
    int idx = GetStartOffs(ch, n, ch_sets);

    for (i = 0; i < m; i++)
    {
        for (j = 0; j < m; j++)
        {
            if (j == i || (i + j) == (m - 1))
                printf("%c", ch_sets[(idx + i) % mod]);
            else
                printf(" ");
        }
        printf("\n");
    }
}
