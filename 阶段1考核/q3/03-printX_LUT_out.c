#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int GetStartOffs(char, int);
void PrintX(char ch, int n);

int main()
{
    int n;
    char ch;

    scanf("%d %c", &n, &ch);

    if (n >= 13 || n <= 0 || ch > 'Z' || ch < 'A')
    {
        printf("Input Data Error.\n");
        return 1;
    }

    PrintX(ch, n);

    return 0;
}

/* 获取起始字符在字符表中的下标索引 */
int GetStartOffs(char ch, int n)
{
    int idx = ch - 'A';

    idx = idx - (n - 1);

    idx = idx < 0 ? idx + 26 : idx;

    return idx;
}

void PrintX(char ch, int n)
{
    int i, j;

    int m = 2 * n -1;

    char *ch_sets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int idx = GetStartOffs(ch, n);

    for (i = 0; i < m; i++)
    {
        for (j = 0; j < m; j++)
        {
            if (j == i || (i + j) == (m - 1))
                printf("%c", ch_sets[(idx + i) % 26]);
            else
                printf(" ");
        }
        printf("\n");
    }
}
