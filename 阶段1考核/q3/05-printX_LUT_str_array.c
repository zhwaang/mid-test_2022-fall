#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int GetStartOffs(char, int);
char ** CreateStringArray(int);
void DestroyStringArray(char **, int);
void SetDiagBackDiag(char **, int, int);
void OutputStr(char **, int);
void PrintX(char, int);

int main()
{
    int n;
    char ch;

    scanf("%d %c", &n, &ch);
    if (n >= 13 || n <= 0 || ch > 'Z' || ch < 'A')
    {
        printf("Input Data Error.\n");
        return 1;
    }
    PrintX(ch, n);
    return 0;
}

/* 获取起始字符在字符表中的下标索引 */
int GetStartOffs(char ch, int n)
{
    int k;

    /* 当前字符在字母表中的下标索引 */
    int idx = ch - 'A';

    /* 计算起始字母索引偏移 */
    idx = idx - (n - 1);

    /* 判断有多少个26 */
    k = abs(idx) / 26;

    /* 如果偏移后的idx为负值，则通过加(k+1)个26按环形结构偏移到正确位置 */
    /* 如果偏移后的idx为正值，则加(k+1)个26不影响结果 */
    idx = (idx + (k + 1) * 26) % 26;

    return idx;
}

/* 申请交错数组(二维动态数组)空间，并化 */
char ** CreateStringArray(int m)
{
    char **s = NULL;
    int i, j;

    /* 申请指针数组 */
    s = (char**)malloc(m * sizeof(char*));
    if(NULL == s)
    {
        exit(1);
    }

    /* 为每个指针申请字符串数组 */
    for(i = 0; i < m; i++)
    {
        s[i] = (char*)malloc((m + 1) * sizeof(char));
        if(NULL == s[i])
        {
            for(j = 0; j < i; j++)
            {
                free(s[j]);
            }
            exit(1);
        }
        /* 初始化为空格 */
        memset(s[i], ' ', m * sizeof(char));
        /* 空字符 */
        s[i][m] = '\0';
    }

    return s;
}

/* 设置主副对角线元素 */
void SetDiagBackDiag(char **s, int idx, int m)
{
    int i;

    /* 查找表 */
    char *ch_sets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /* 主副对角线赋值 */
    for (i = 0; i < m; i++)
    {
        s[i][i] = ch_sets[(idx + i) % 26];
        s[i][(m - 1 - i)] = ch_sets[(idx + i) % 26];
    }
}

/* 输出结果 */
void OutputStr(char **s, int m)
{
    int i;

    /* 逐个输出字符串 */
    for (i = 0; i < m; i++)
    {
        puts(s[i]);
    }
}

/* 销毁内存 */
void DestroyStringArray(char **s, int m)
{
    int i;
    /* 释放空间 */
    for(i = 0; i < m; i++)
    {
        free(s[i]);
    }
    free(s);
}

void PrintX(char ch, int n)
{
    char **s = NULL;

    /* 总行列数(方阵) */
    size_t m = (2 * n - 1);

    /* 起始字符在查找表中的位置 */
    int idx = GetStartOffs(ch, n);

    /* 创建记录结果的字符串数组(交错数组) */
    s = CreateStringArray(m);

    SetDiagBackDiag(s, idx, m);

    OutputStr(s, m);

    /* 销毁空间 */
    DestroyStringArray(s, m);
}
