#include <stdio.h>

void printX(char ch, int n);
char outChar(int n);


int main(void)
{
    int n;
    char ch;

    scanf("%d %c", &n, &ch);

    if (n >= 13 || n <= 0 || ch > 'Z' || ch < 'A')
    {
        printf("Input Data Error.\n");

        return 1;
    }
    printX(ch, n);

    return 0;
}

char outChar(int n)
{
    if (n > 'Z')
        return n - 26;
    else if (n < 'A')
        return n + 26;
    else
        return n;
}

void printX(char ch, int n)
{
    int i, j;
    int m = 2 * n - 1;

    ch = ch - (n - 1);

    for (i = 0; i < m; i++)
    {
        for (j = 0; j < m; j ++)
        {
            if (j == i || (i + j) == (m - 1))
                printf("%c", outChar(ch + i));
            else
                printf(" ");
        }
        printf("\n");
    }
}
