#include <stdio.h>
#include <string.h>

#define MAX_LEN 256

char Encryption(char , int);

int main()
{
    int i, len, k = 0;
    char Old_msg[MAX_LEN] = {0}, Enc_msg[MAX_LEN] = {0};

    fgets(Old_msg, MAX_LEN, stdin);
    len = strlen(Old_msg);
    for(i=0;i<len;i++)
    {
        Enc_msg[i] = Encryption(Old_msg[i], k++);
        k %= 10;
    }
    Enc_msg[i] = '\0';
    printf("%s", Enc_msg);
    return 0;
}

/* 以上代码学生不可见 */
int get_index(char ch, const char *s)
{
    int i;

    for(i = 0; i < 52; i++)
    {
        if(s[i] == ch)
        {
            return i;
        }
    }

    return -1;
}

int get_encoded_index(int idx, int k)
{
    if(idx >= 26)
    {
        return (idx + k) % 26 + 26;
    }
    else
    {
        return (idx + k) % 26;
    }
}

char Encryption(char ch, int k)
{
    char *alpha = "abcdefghijklmnopqrstuvwxyz"
                  "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int or_idx;
    int en_idx;

    or_idx = get_index(ch, alpha);
    if(or_idx == -1)
    {
        return ch;
    }

    en_idx = get_encoded_index(or_idx, k);

    return alpha[en_idx];
}
