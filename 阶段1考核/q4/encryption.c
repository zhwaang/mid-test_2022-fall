#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 256
char Encryption(char, int);

int main(void)
{
    int i, len, k = 0;
    char msg[MAX_LEN] = {0}, enc_msg[MAX_LEN] = {0};

    fgets(msg, MAX_LEN, stdin);

    len = strlen(msg);

    for (i = 0; i < len; i ++)
    {
        enc_msg[i] = Encryption(msg[i], k++);
        k %= 10;
    }
    enc_msg[i] = '\0';

    printf("%s", enc_msg);

    return 0;
}

char Encryption(char ch, int k)
{
    if (ch <= 'z' && ch >= 'a')
    {
        ch = 'a' + (ch - 'a' + k) % 26;
    }
    else if (ch <= 'Z' && ch >= 'A')
    {
        ch = 'A' + (ch - 'A' + k) % 26;
    }

    return ch;
}
