#include <stdio.h>

int Sum_Septenary(int);
int Is_Prime(int);

int main()
{
     int n;

     scanf("%d", &n);
     printf("%d\n", Sum_Septenary(n));
     printf("%d,%d\n", Is_Prime(n), Is_Prime(Sum_Septenary(n)));
     return 0;
}

/* 以上代码学生不可见 */
int Sum_Septenary(int n)
{
    if(n < 7)
    {
        return n;
    }

    return Sum_Septenary(n / 7) + ( n % 7 );
}

int Is_Prime(int n)
{
    int i;

    if (n <= 3 && n > 0)
    {
        return 1;
    }

    for(i = 2;i * i <= n; i++)
    {
        if ( ! (n % i))
        {
            return 0;
        }
    }

    return 1;
}
