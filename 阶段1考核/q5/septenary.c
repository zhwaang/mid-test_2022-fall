#include <stdio.h>

int Sum_Septenary(int);
int Is_Prime(int);

int main(void)
{
    int n;

    scanf("%d", &n);

    printf("%d\n", Sum_Septenary(n));

    printf("%d,%d\n", Is_Prime(n), Is_Prime(sumSep(n)));

    return 0;
}

int Sum_Septenary(int n)
{
    int sum = 0;

    while (n != 0)
    {
        sum = sum + n % 7;
        n = n / 7;
    }
    return sum;
}

int Is_Prime(n)
{
    int i;

    if (n <= 3 && n > 0)
    {
        return 1;
    }

    for (i = 2; i * i <= n; i ++)
    {
        if (!(n % i))
        {
            return 0;
        }
    }

    return 1;
}
