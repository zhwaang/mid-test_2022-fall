/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：颠倒句子中单词，用头插法创建的链表实现。
*       首先用strtok切分出句子中的单词，并记录每个单词首地址后，用头插法插入链表。
*       然后遍历链表得到逆序后的单词(头插入法链表具有先入后出的特性)。
*       将单词拼接成结果句子字符串。
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年11月26日
*
* 取代版本：无
* 原作者：耿楠
* 完成日期：
------------------------------------------------------------------------------------*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* 记录单词结构体 */
typedef struct _word
{
    char *wd;           /* 单词字符串指针 */
    size_t len;         /* 单词长度 */
    struct _word *next; /* 指向下一个单词的指针 */
}word;

/* 链表操作 */
word *createWordList(char*, const char*);
word *pushWord(word*, char *);
char *popWord(word**);

/* 判断是否为一个句子 */
int isSentence(const char *, const char *, const char *);
/* 颠倒句子中的单词 */
char* revSentenceWords(char *, const char *, const char *, const char *);

/* 错误响应 */
static void errormsg(const char *);

int main()
{
    /* 句子 */
    /* char s[] = "you can cage a swallow can't you?"; */
    char s[] = "   you can \n  cage a \tswallow  \ncan't you?   ";

    /* 原句子单词分隔符 */
    char delim[] = " \t\n\r";
    /* 句子终止符 */
    char term[] = ".?!";
    /* 结果句子单词分隔符 */
    char hyphen[] = " ";
    char *str = NULL;

    if(isSentence(s, delim, term))
    {
        str = revSentenceWords(s, delim, term, hyphen);
        if(str != NULL)
        {
            puts(str);
        }
    }

    return 0;
}

/* 错误响应 */
static void errormsg(const char *message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}

/*------------------------------------------------------------------------
// 名称：int isSentence(const char *s, const char *delim, const char *term)
// 功能：判断句子是否有句子终止字符。
// 算法：找到句子尾部不是分隔符(空白)的字符，判断其是不是句子终止字符。
// 参数：
//       [char* s] -------------- 待逆序的字符串的指针
//       [const char* delim] ---- 单词单词分隔字符集指针
//       [const char* term] ----- 句子终止字符集指针
// 返回：[int] --- 有则返回1, 无则返回0
// 作者：耿楠
// 日期：2022年11月26日
------------------------------------------------------------------------*/
int isSentence(const char *s, const char *delim, const char *term)
{
    size_t len = 0;

    len = strlen(s);

    /* 删除尾部分隔符(空白) */
    len = strlen(s) - 1;
    while(strchr(delim, s[len]) && len >= 0)
    {
        len--;
    }

    /* 判断最后有没有句子终止字符 */
    if(!strchr(term, s[len]))
    {
        return 0;
    }

    return 1;
}

/*------------------------------------------------------------------------
// 名称：char* revSentenceWords(char *s, const char *delim,
//                              const char *term, const char *hyphen)
// 功能：将句子按单词逆序(单词必须以空格分隔)。
// 算法：首先用头插法创建一个单词链表(单词仍在原字符串)，在每个结点记录单词
//       基本信息的长度和指针，指针指向原字符串的对应位置。最后遍历链表，
//       在原字符串空间拼接结果字符串。
// 参数：
//       [char* s] -------------- 待逆序的字符串的指针
//       [const char* delim] ---- 单词单词分隔字符集指针
//       [const char* term] ----- 句子终止字符集指针
//       [const char* hyphen] --- 结果单词分隔字符集指针
// 返回：[char*] --- 逆序后字符串的指针
// 作者：耿楠
// 日期：2022年11月26日
// 注意：处理过程会对原字符串进行改写，如需要保留原串，请在调用该函数前备份。
------------------------------------------------------------------------*/
char* revSentenceWords(char *s, const char *delim,
                       const char *term, const char *hyphen)
{
    char *str, *p;
    char terminator = 0;
    size_t str_len = strlen(s);

    /* 单词链表 */
    word *word_list = NULL;

    str = (char*)malloc((str_len + 1) * sizeof(char));
    if(str == NULL)
    {
        errormsg("Not enough memory for reverse temp string.");
    }
    strcpy(str, s);

    /* 记录句子终止字符并删除原句子终止字符 */
    p = str + str_len - 1;
    while(!strchr(term, *p))
    {
        p--;
    }
    terminator = *p;
    *p = '\0';

    /* 构建单词链表 */
    word_list = createWordList(str, delim);

    /* 清空原串 */
    *s = '\0';

    /* 弹出链表中的字符串，并拼接结果字符串 */
    while(word_list != NULL)
    {
        strcat(s, popWord(&word_list));
        strcat(s, hyphen);
    }

    /* 添加句子终止字符 */
    str_len = strlen(s);
    s[str_len - 1] = terminator;

    free(str);

    return s;
}

/*------------------------------------------------------------------------
// 名称：word *createWordList(char *s, const char *delim)
// 功能：根据传入的句子用strtok函数切分单词后用头插法创建单词链表。
// 参数：
//       [char* s] -------------- 待逆序的字符串的指针
//       [const char* delim] ---- 单词单词分隔字符集指针
// 返回：[word*] --- 单词链表头指针
// 作者：耿楠
// 日期：2022年11月26日
------------------------------------------------------------------------*/
word *createWordList(char *s, const char *delim)
{
    word *phead = NULL;

    char *str;

    /* 用strtok函数切分字符串，将每个单词的基本信息插入链表 */
    str  = strtok(s, delim);
    while(str != NULL)
    {
        phead = pushWord(phead, str);
        str = strtok(NULL, delim);
    }

    return phead;
}

/*------------------------------------------------------------------------
// 名称：word *pushWord(word *plist, char *str)
// 功能：根据指定的字符串创建单词结点并用头插法插入链表。
// 参数：
//       [word* plist] --- 链表头指针
//       [char* str] ----- 结点字符串指针
// 返回：[word*] --- 单词链表头指针
// 作者：耿楠
// 日期：2022年11月26日
------------------------------------------------------------------------*/
word *pushWord(word *plist, char *str)
{
    word *pnew_word = NULL;

    /* 创建新单词 */
    pnew_word = (word*)malloc(sizeof(word));
    if(pnew_word == NULL)
    {
        return plist;
    }

    /* 用头插法插入链表 */
    pnew_word->wd = str;
    pnew_word->len = strlen(str);
    pnew_word->next = plist;

    return pnew_word;
}

/*------------------------------------------------------------------------
// 名称：char *popWord(word **plist)
// 功能：取得链表头结点单词指针，然后删除头结点，并调整头结点。
// 参数：
//       [word** plist] --- 链表头指针
// 返回：[char*] --- 单词指针
// 作者：耿楠
// 日期：2022年11月26日
// 备注：由于需要修改链表头指针，因此必须使用指针的指针做为形参。
------------------------------------------------------------------------*/
char *popWord(word **plist)
{
    word *p_top;
    char *s;

    /* 链表为空，返回NULL */
    if((*plist) == NULL)
    {
        return NULL;
    }

    /* 记录链表头结点指针 */
    p_top = (*plist);
    s = p_top->wd;

    /* 调整链表头指针 */
    (*plist) = (*plist)->next;

    /* 删除链表头结点 */
    free(p_top);

    return s;
}
