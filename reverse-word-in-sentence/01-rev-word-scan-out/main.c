/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：颠倒句子中单词，简单读入字符记录分隔符结束位置实现。
*       在读入句子各个字符后，记录最后位置。
*       反向遍历，字符单词起始位置，然后逐字符输出各个单词。
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年11月26日
*
* 取代版本：无
* 原作者：耿楠
* 完成日期：
------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 最大句子长度 */
#define LEN 50

/* 函数原型 */
int get_sentence(char*, const char*, const char*, char *, int);
void rev_print_sentence_words(char*, int, char, char);

int main(void)
{
    /* 句子数组 */
    char sentence[LEN + 1] = {0};

    /* 句子单词分隔符集 */
    char delim[] = " \t\n\r";
    /* 句子终止符集 */
    char term[] = ".?!";

    /* 结果句子单词分隔符 */
    char hyphen = '-';
    /* 结果句子终止符 */
    char terminator = 0;

    /* 最后一个分隔符位置 */
    int last_delim_pos;

    /* 读入句子 */
    last_delim_pos = get_sentence(sentence, delim, term, &terminator, LEN);

    /* 颠倒打印各个单词 */
    rev_print_sentence_words(sentence, last_delim_pos, hyphen, terminator);

    return 0;
}

/*--------------------------------------------------------------------
// 名称：int get_sentence(char *sentence, const char * delim,
//                        const char *term, char *terminator, int n)
// 功能：读入需要颠倒单词的句子。
// 算法：逐个读入字符，直到遇到句子终止符为止，并将终止符写入传入的
//       终止符指针指向的变量。如果读入的字符为分隔符，将其转换为空格。
// 参数：
//       [char *sentence] ------ 存储句子字符的数组
//       [const char *delim] --- 单词分隔字符集
//       [const char *term] ---- 句子终止字符集
//       [char *terminater] ---- 存储句子终止符变量的指针
//       [int n] --------------- 句子最大长度
// 返回：[int] --- 最后一个字符后的位置
// 作者：耿楠
// 日期：2022年11月26日
---------------------------------------------------------------------*/
int get_sentence(char *sentence, const char * delim, const char *term,
                 char *terminator, int n)
{
    int ch;
    int i, last_space = 0;

    i = 0;
    ch = getchar();
    /* 不是'\n'和EOF则一直读入 */
    while(ch != '\n' && ch != EOF && i < n)
    {
        /* 是句子终止符，记录数据后返回 */
        if(strchr(term, ch))
        {
            last_space = i;
            *terminator = ch;
            break;
        }
        /* 是分隔字符集中的字符，转换为空格 */
        if(strchr(delim, ch))
        {
            sentence[i] = ' ';
        }
        else
        {
            sentence[i] = ch;
        }

        ch = getchar();
        i++;
    }

    return last_space;
}

/*--------------------------------------------------------------------
// 名称：void rev_print_sentence_words(char *sentence, int last_space,
//                                     char hyphen, char terminator)
// 功能：颠倒句子中的单词
// 算法：从后向前，反向找到空格分隔符，再从空格字符开始正向遍历每字符，
//       如果不是空格，则输出该字符。同时，添加单词分隔符和句子终止符。
// 参数：
//       [char *sentence] ---- 存储句子的数组
//       [int last_space] ---- 最后一个单词后的位置
//       [char hyphen] ------- 结果句子单词分隔符
//       [char terminater] --- 结果句子终止符
// 返回：[void] --- 无
// 作者：耿楠
// 日期：2022年11月26日
---------------------------------------------------------------------*/
void rev_print_sentence_words(char *sentence, int last_space,
                              char hyphen, char terminator)
{
    int i, j;
    int flag = 0;
    int off = 0;
    char *s = sentence;

    /* 跳过前导空格 */
    while(*sentence == ' ')
    {
        sentence++;
    }
    /* 计算跳过的偏移量 */
    off = sentence - s;
    /* 调整结尾字符位置 */
    last_space -= off;

    /* 从后向前反向遍历 */
    for(i = last_space; i > 0; i--)
    {
        /* 发现空格(单词分隔符) */
        if(sentence[i] == ' ')
        {
            /* 逐个字符输出整个单词 */
            for(j = i + 1; j < last_space; j++)
            {
                flag = 1;
                putchar(sentence[j]);
            }
            /* 输出了单词，添加分隔符 */
            if(flag)
            {
                flag = 0;
                putchar(hyphen);
            }

            /* 跳过多余空格 */
            while(sentence[i] == ' ')
            {
                i--;
            }
            i++;
            last_space = i;
        }
    }

    /* 处理最后一个单词 */
    while(sentence[i] != '\0' && sentence[i] != ' ')
    {
        putchar(sentence[i++]);
    }

    /* 输出句子终止符 */
    printf("%c\n", terminator);
}
