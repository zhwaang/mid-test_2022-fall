/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：颠倒句子中单词，用记录单词起始位置索引和长度的结构体数组实现。
*       首先使用strtok函数实现单词切分，得到每个单词在原字符串中的起始索引和长度。
*       然后构造结构体数组记录每个单词的基本信息。
*       最后反向遍历结构数组，根据每个单词的基本信息在取得各单词后实现结果拼接。
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年11月26日
*
* 取代版本：无
* 原作者：耿楠
* 完成日期：
------------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* 字义记录一个单词信息的结构体 */
typedef struct
{
    int start;  /* 单词在原串中的起始索引 */
    int length; /* 单词长度 */
}word;

/* 函数原型 */
size_t createWordSet(char *, const char *, word **);
char* revSentenceWords(char *, const char *, const char *, const char *);
/* 判断是否为一个句子 */
int isSentence(const char *, const char *, const char *);

/* 错误响应 */
static void errormsg(const char *);

int main()
{
    /* 句子 */
    /* char s[] = "you can cage a swallow can't you?"; */
    char s[] = "   you can \n  cage a \tswallow  \ncan't you?   ";

    /* 原句子单词分隔符 */
    char delim[] = " \t\n\r";
    /* 句子终止符 */
    char term[] = ".?!";
    /* 结果句子单词分隔符 */
    char hyphen[] = " ";

    char *str = NULL;

    if(!isSentence(s, delim, term))
    {
        errormsg("Sentence needs a terminating character. (./?/!)");
    }

    str = revSentenceWords(s, delim, term, hyphen);
    puts(str);

    return 0;
}

/* 错误响应 */
static void errormsg(const char *message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}

int isSentence(const char *s, const char *delim, const char *term)
{
    size_t len = 0;

    len = strlen(s);

    /* 删除尾部分隔符(空白) */
    len = strlen(s) - 1;
    while(strchr(delim, s[len]) && len >= 0)
    {
        len--;
    }

    /* 判断最后有没有句子终止字符 */
    if(!strchr(term, s[len]))
    {
        return 0;
    }

    return 1;
}

/*------------------------------------------------------------------------
// 名称：size_t createWordSet(char *s, const char *delim, word **wordset)
// 功能：创建单词结构体动态数组(单词集合)。
// 算法：根据分隔符定位单词并得到一个单词的起始地址和长度记入结构体成员，
//       利用strtok函数实现字符串切分。
// 参数：
//       [char* s] ------------- 待处理字符串的指针
//       [const char* delim] --- 单词单词分隔字符集指针
//       [word **wordset] ------ 单词信息集合指针
// 返回：[size_t] --- 总计单词数
// 作者：耿楠
// 日期：2022年11月26日
------------------------------------------------------------------------*/
size_t createWordSet(char *s, const char *delim, word **wordset)
{
    int cnt;

    char *p, *str = s;
    word *w;
    size_t len;

    /* 假设有字符长度个单词(最大) */
    len = strlen(s);
    w = (word *)malloc(len * sizeof(word));
    if(w == NULL)
    {
        errormsg("Not enough memory for word's set.");
    }

    /* 变量初值 */
    cnt = 0;
    p = s;

    /* 得到第一个单词 */
    p = strtok(s, delim);
    while(p != NULL)
    {
        /* 记录一个单词的信息 */
        w[cnt].length = strlen(p);
        w[cnt].start = p - str;
        cnt++;

        /* 取得下一个单词 */
        p = strtok(NULL, delim);
    }

    /* 重新分配内存，避免浪费空间 */
    w = (word *)realloc(w, cnt * sizeof(word));
    if(w == NULL)
    {
        errormsg("Not enough memory for word's set realloc.");
    }

    /* 将单词信息动态数组首地址写回 */
    *wordset = w;

    /* 返回单词总数 */
    return cnt;
}

/*------------------------------------------------------------------------
// 名称：char* revSentenceWords(char *s, const char *delim, const char *term, const char *hyphen)
// 功能：将句子按单词逆序(单词必须以空格分隔)。
// 算法：首先创建一个单词信息结构体动态数组，记录每个单词在字符串中的起始
//       地址和长度，然后遍历结构体动态数组在原字符串空间拼接结果字符串。
// 参数：
//       [char* s] -------------- 待逆序的字符串的指针
//       [const char* delim] ---- 单词单词分隔字符集指针
//       [const char* term] ----- 句子终止字符集指针
//       [const char* hyphen] --- 结果单词分隔字符集指针
// 返回：[char*] --- 逆序后字符串的指针
// 作者：耿楠
// 日期：2022年11月26日
// 注意：处理过程会对原字符串进行改写，如需要保留原串，请在调用该函数前备份。
------------------------------------------------------------------------*/
char* revSentenceWords(char *s, const char *delim, const char *term, const char *hyphen)
{
    size_t words_cnt = 0;
    size_t str_len = strlen(s);

    char *str, *p;
    word *w = NULL;
    char terminator = 0;
    int i, j;

    str = (char*)malloc((str_len + 1) * sizeof(char));
    if(str == NULL)
    {
        errormsg("Not enough memory for reverse temp string.");
    }
    strcpy(str, s);

    /* 记录句子终止字符并删除原句子终止字符 */
    p = str + str_len - 1;
    while(!strchr(term, *p))
    {
        p--;
    }

    terminator = *p;
    *p = '\0';

    /* 创建单词集合，注意调用该函数前需要对字符首尾空白进行修剪 */
    words_cnt = createWordSet(str, delim, &w);

    /* 遍历结构体数组，逐字符实现单词逆序后写入原字符串 */
    p = s;
    for(i = words_cnt - 1; i >= 0; i--)
    {
        for(j = 0; j < w[i].length; j++)
        {
            *p++ = str[w[i].start + j];
        }
        /* 写入单词分隔符 */
        *p++ = hyphen[0];
    }

    /* 处理句子终止符和字符串终止字符 */
    *p = '\0';
    p--;
    *p = terminator;

    /* 释放内存 */
    free(w);
    free(str);

    return s;
}
