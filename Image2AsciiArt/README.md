# BMP位图转字符位图
----------------
BMP位图是一种常用的位图图像格式，它结构简单，操作方便。在此要求将一幅位图按指定的区块大小，用文本字符进行表达该区块，以生成字符位图。

分析该题目的题意可以看出，主要需要完成的工作有：
1. 读取BMP位图数据。
2. 按指定的区块大小和处理方式，计算表达区块的颜色值。
3. 在查找到中检索与该颜色值对应的字符，并在指定的区块位置输出该字符。
4. 如有需要，可以将字符位图写入文本文件。

## BMP位图文件结构
----------------
一幅平面位图图像，在计算机中最终呈现的是一个彩色的二维矩阵。BMP图像文件是一种用于保存位图的文件格式，它由文件头、位图信息头、调色板(可选)和图像数据4部分构成。
|  名称  |字节数|                               功能                                   |
| :----: |:----:|                              :----                                   |
|文 件 头|  14  |文件类型、大小、数据起始地址等信息                                    |
|信 息 头|  40  |图像尺寸、压缩、颜色等信息                                            |
|颜 色 表| 不定 |颜色查找表，大小根据每像素位数确定，1/4/8/24/32分别对应2/16/256/0/0项 |
|位图数据| 不定 |位图实际数据，以从左向右，从下向上的方式记录位图数据                  |

#### 文件头
----------------
BMP位置文件头由14个字节构成，用于描述BMP文件的基本信息。
|起始地址 | 长度 |                说明           |    示例    |
| :------:|:----:|               :----           |    :----   |
|  0X0000   |  2  | 固定为'B''M'，表示Windows的BMP | 0X424D     |
|  0X0002   |  4  | 文件总长度                     | 0X00007A30 |
|  0X0006   |  2  | 保留字1                        | 0X0000     |
|  0X0008   |  2  | 保留字2                        | 0X0000     |
|  0X000A   |  4  | 数据区起始地址                 | 0X00007A00 |

在C语言中，可以用结构体实现对文件头的存储和管理：
```c
/* 文件头结构体定义 */
typedef struct
{
    unsigned short int type;       /* 标识                    */
    unsigned int       size;       /* 文件大小(bytes          */
    unsigned short int reserved1;  /* 保留字1                 */
    unsigned short int reserved2;  /* 保留字2                 */
    unsigned int       offset;     /* 图像数据偏移地址(bytes) */
} FILEHEADER;
```

#### 图像信息头
----------------
BMP位置文件头由40个字节构成，用于描述BMP文件的基本信息。
| 起始地址 | 长度 |         说明             |    示例    |
| :------: |:----:|         :----            |    :----   |
|  0X000E    |  4   | 位图信息区块的长度<ul><li>0X28-Windows 3.1x, 95, NT, ...</li><li>0X0C-OS/2 1.x</li><li>0XF0-OS/2 2.x</li></ul>  | 0X00000028 |
|  0X0012    |  4   | 图像宽度(像素)           | 0X00000008 |
|  0X0016    |  4   | 图像高度(像素)           | 0X00000005 |
|  0X001A    |  2   | 颜色平面数               | 0X0001     |
|  0X001C    |  2   | 颜色深度<ul><li>1-单色图像</li><li>4-16色图像</li><li>8-256色图像</li><li>16-64K图像</li><li>24-16M色图像</li><li>32-16M色图像(带alpha透明通道)</li></ul> | 0X0018     |
|  0X001E    |  4   | 压缩类型<ul><li>0/BI_RGB-不压缩</li><li>1/BI_RLE4-8位/像素RLE压缩</li><li>2/BI_RLE8-4位/像素RLE压缩</li><li>3/BI_BITFIELDS-Bitfields压缩</li> </ul>| 0X00000000 |
|  0X0022    |  4   | 位图数据尺寸(字节数)，必须是4的整数倍 | 0X00000078 |
|  0X0026    |  4   | 水平分辨率(像素/米)      | 0X00000000 |
|  0X002A    |  4   | 垂直分辨率(像素/米)      | 0X00000000 |
|  0X002E    |  4   | 颜色索引数               | 0X00000000 |
|  0X0032    |  4   | 重要颜色数               | 0X00000000 |

在C语言中，可以用结构体实现对文件头的存储和管理：
```c
/* 图像信息头结构体定义 */
typedef struct
{
    unsigned int size;             /* 信息头大小(bytes)   */
    int width, height;             /* 图像宽和高(pixels)  */
    unsigned short int planes;     /* 颜色平面            */
    unsigned short int bits;       /* 颜色深度            */
    unsigned int compression;      /* 压缩类型            */
    unsigned int imagesize;        /* 图像数据大小(bytes) */
    int xresolution, yresolution;  /* 单位长度像素数      */
    unsigned int ncolours;         /* 颜色数              */
    unsigned int importantcolours; /* 重要颜色数          */
} INFOHEADER;
```

#### 调色板
----------------
仅当图像的颜色深度分别是1、4、8时，才需要调色板，对应的是单色、16色和256色的调色析，相对应的调色板大小是2、16和256，调色板以4字节为单位，每4个字节存放一个颜色值，此时，图像数据是指向调色板的颜色索引值。

每个调色板项的大小为4字节，按蓝、绿、红顺序存储一个颜色值。

| 起始地址 | 长度 |         说明             |    示例    |
| :------: |:----:|         :----            |    :----   |
|  0X0036    | N\*4 | 调色板，对于每项用4字节表示RGB值：<ul><li>1字节蓝色分量</li><li>1字节绿色分量</li><li>1字节红色分量<li>1字节填充0</li></ul>  | 0X2A00FF00 |

当图像的颜色深度分别是16、24和32时，则无调色板数据。图像像素的颜色值直接在位图数据中给出。

如果图像带有调色板，则位图数据可以根据需要选择压缩与不压缩，如果选择压缩，则根据BMP图像是16色或256色，采用RLE4或RLE8压缩算法压缩。

#### 图像数据
----------------
如果有调色析，则图像数据位于调色板之后，如果没有调色板，则图像数据的起始地址就是`0X0036`。
| 起始地址 | 长度 |         说明             |    示例    |
| :------: |:----:|         :----            |    :----   |
|  0X0436  | xx   | 实际图像数据，取决于有无调色板，压缩方式等  | 0X2A00FF00 |

当图像具有调色板时，图像数据是按行从左到右，从下到上的顺序逐像素存储的是调色板中的颜色索引值。

当颜色深度为1时，图像数据的每1位表示一个像素，一个字节可以表示8个像素，按0或1分别取出调色板颜色。此时，调色板中只有两种颜色，也就是通常说的黑白照片。

当颜色深度为4时，图像数据用4位表示一个像素，一个字节可以表示2个像素，计算后得到调色板索引值。此时，可以表示16种颜色。

当颜色深度为8时，图像数据用8位表示一个像素，一个字节表示1个像素，计算后得到调色板索引值。此时，可以表示256种颜色。

当颜色深度为16时，使用2个字节表示一个像素，常用555(5位蓝5位绿5位红)和565(5位蓝6位绿5位红)的格式表示一种颜色。此时，可以表示2^16=65536种颜色。

当颜色深度为24时，使用3个字节表示一个像素，分别表示一种颜色的蓝、绿、红颜色分量。此时，可以表示2^24=16 777 216种颜色。

当颜色深度为32时，使用4个字节表示一个像素，分别表示一种颜色的蓝、绿、红颜色分量，并用另一个字节表示其Alpha透明色。此时，可以表示2^24=16 777 216种颜色。

**注意：**
1. 图像数据是按行从左向右，从下向上存储的。因此，如果处理不当，则图像会出现上下颠倒的现象。
2. 图像的颜色三分量顺序是按蓝、绿、红的顺序存储的。
3. 每一行图像数据占有的字节数必须是4的整数倍，当不足4的整数倍时，需要使0进行补齐。常用的其补齐算法有：
    - 算法1：`widthbytes = (width * bits + 31) / 32 * 4`
    - 算法2：`widthbytes = ((width * bits + 31) & ~31) / 8`
    - 算法3：`widthbytes = ((width * bits + 31) & ~31) >> 3`

## 颜色结构体
----------------
为便于处理一个像素的颜色，可以用一个结构体表示一种颜色的红、绿、蓝和alpha通道分量(注意颠倒顺序)：
```c
/* 图像数据信息结构体定义 */
typedef struct
{
    unsigned char b; /* 蓝 */
    unsigned char g; /* 绿 */
    unsigned char r; /* 红 */
    unsigned char a; /* alpha(预留) */
} RGB;
```

## 结构体字节对齐
----------------
由于读写效率的原因，一个结构体的大小(占有的字节数)有可能不等于结构体内部成员变量所占内存的总和，可能会补齐到4的整数倍。而在读写BMP图像文件时，文件头、图像信息头等数据时，又必须严格按14、40等字节数进行读取，因此需要在定义结构体时，限定其按单字节对齐，以避免产生字节补齐现象。

为此，可以使用`#pragma pack(1)`宏指令设置结构体按单字节对齐，但要注意对于不需要的结构体需要取消该设置以提升工作效率。

```c
/* 保存当前结构体字节对齐状态 */
#pragma pack(push)
/* 设置结构体字节对齐为单字节对齐 */
#pragma pack(1)
/* 文件头结构体定义 */
typedef struct
{
    ...
} FILEHEADER;

/* 图像信息头结构体定义 */
typedef struct
{
    ...
} INFOHEADER;

/* 图像数据信息结构体定义 */
typedef struct
{
    ...
} RGB;
/* 恢复结构体字节对齐状态 */
#pragma pack(pop)
```

## 图像数据结构体
----------------
在实际处理图像数据时，往往只需要关注图像的宽度、高度、字节宽度(补齐后的宽度)和具体的图像数据，因此，可以用一个结构体表示数据处理中的幅数字图像：
```c
/* 图像数据信息结构体定义 */
typedef struct
{
    unsigned int width;      /* 位图的宽度，以像素为单位 */
    unsigned int height;     /* 位图的高度，以像素为单位 */
    unsigned int widthbytes; /* 位图的字节宽度           */
    unsigned char *data;     /* 图像数据指针             */
} BMP_Data;
```

## 读取图像数据
----------------
在明确了图像文件结构和定义了需要的结构体后，便可以实现图像数据的读取。

为了简化处理，在此，**仅处理24位的BMP图像文件**。

#### 打开图像文件
----------------
由于BMP图像文件是以二进制方式存储的，因此，需要以二进制的方式打开一个BMP图像文件：
```c
    /* 打开图像文件 */
    if(!(bmp_file = fopen(file_name, "rb")))
    {
        return 0;
    }
```

#### 读入图像文件头
----------------
打开BMP图像文件后，便可以利用文件头结构体读取BMP图像文件的文件头数据：
```c
    /* 读入文件头数据 */
    if(sizeof(fHeader) == 14)
    {
        fread( &fHeader, sizeof(fHeader), 1, bmp_file);
    }
    else
    {
        fread( &fHeader.type,      2, 1, bmp_file);
        fread( &fHeader.size,      4, 1, bmp_file);
        fread( &fHeader.reserved1, 2, 1, bmp_file);
        fread( &fHeader.reserved2, 2, 1, bmp_file);
        fread( &fHeader.offset,    4, 1, bmp_file);
    }
```
在读取文件头时，可以根据结构体的尺寸是否为14字节以确定是整块读取还是逐项读取。

#### 读入图像信息头
----------------
在读取了BMP图像文件头后，便可继续读取BMP图像信息头数据：
```c
    /* 读入图像头数据 */
    if(sizeof(bmpHeader) == 40)
    {
        fread( &bmpHeader, sizeof(bmpHeader), 1, bmp_file);
    }
    else
    {
        fread( &bmpHeader.size,             4, 1, bmp_file);
        fread( &bmpHeader.width,            4, 1, bmp_file);
        fread( &bmpHeader.height,           4, 1, bmp_file);
        fread( &bmpHeader.planes,           2, 1, bmp_file);
        fread( &bmpHeader.bits,             2, 1, bmp_file);
        fread( &bmpHeader.compression,      4, 1, bmp_file);
        fread( &bmpHeader.imagesize,        4, 1, bmp_file);
        fread( &bmpHeader.xresolution,      4, 1, bmp_file);
        fread( &bmpHeader.yresolution,      4, 1, bmp_file);
        fread( &bmpHeader.ncolours,         4, 1, bmp_file);
        fread( &bmpHeader.importantcolours, 4, 1, bmp_file);
    }
```
同样，在读取信息头时，可以根据结构体的尺寸是否为40字节以确定是整块读取还是逐项读取。

#### BMP图像格式合法性判断
----------------
在读入图像基本数据后，便可以判断是否为24位真彩色BMP图像，如果不是24位真彩色BMP图像，则无法进行后续处理：
```c
    /* 仅处理24位BMP图像 */
    if( *((char *)&fHeader.type) != 'B' &&
        *(((char *)&fHeader.type) + 1) != 'M' &&
        bmpHeader.bits != 24 )
    {
        fclose( bmp_file );
        return 0;
    }
```

#### 每行字节宽度计算
----------------
对于24位真彩色BMP图像，则可以用如下简化算法实现每行数据字节宽度的计算：
```c
    /* 实际每行存储字节数 */
    bmp_data->widthbytes = ((bmp_data->width * 3) % 4 == 0) ?
                           (bmp_data->width * 3) : ((bmp_data->width * 3) / 4 * 4 + 4);
```

#### 读入数像数据
----------------
在完成图像每行字节宽度计算后，便可以得到图像数据占有的字节数，从而实现图像数据的读入：
```c
    /* 位图数据大小(字节) */
    unsigned int size = bmp_data->widthbytes * bmpHeader.height * 3;

    /* 为存储图像数据分配内存 */
    bmp_data->data = (unsigned char *)malloc( size );

    /* 调整文件指针到图像数据区 */
    fseek( bmp_file, fHeader.offset, SEEK_SET);
    fread( bmp_data->data, size, 1, bmp_file);
```

在得到图像宽度、高度、字节宽度和图像数据后，便可以进行后续的处理了。

为简化代码结构，对于BMP图像文件的打开、读取等操作，可以将其封装为一个`int Load_BMP(const char *file_name, BMP_Data *bmp_data)`函数。

## 行数据翻转
----------------
由BMP图像文件数据格式可知，其图像存储时，各行是上下颠倒的，因此需先对其按行进行翻转：
```c
    /* 图像数据高度方向对调 */
    for(i = 0; i < hf; i++)
    {
        memcpy(p, q + i * bmp_data->widthbytes, colsize);
        memcpy(q + i * bmp_data->widthbytes, q + (hb - i) * bmp_data->widthbytes, colsize);
        memcpy(q + (hb - i) * bmp_data->widthbytes, p, colsize);
    }
```
同样，为简化代码结构，可将其封装为一个`int FlipUpDown(BMP_Data *bmp_data)`函数。

## 读取指定位置像素值
----------------
在读取BMP图像数据后，该数据就是一个用`unsigned char`一维数组模拟的二维数组。通过图像像素的x、y坐标，便可以计算得到其数据偏移量，连续读入3个字节，便可以读入一个像素的红、绿、蓝颜色分量：
```c
    /* 图像数据区首指针 */
    p = bmp_data->data;
    /* 偏移至第y行 */
    p += y * bmp_data->widthbytes;
    /* 偏移至第x列 */
    p += x * 3;

    /* 读像素值(三个字节) */
    memcpy(&temprgb, p, 3);
```
同样，为简化代码结构，可将其封装为一个`RGB GetPixel(int x, int y, BMP_Data *bmp_data)`函数。

## ASCII字符图像生成
----------------
将BMP图像转换为ASCII字符图像的原理按指定的区块宽度和高度取出图像像素值，然后按指定的算法计算出该区块的特征值，再以该特征值为下标索引在ASCII字符查找表中找到对应的字符。

#### ASCII字符查找表
----------------
可以根据需要定义字符查找表，如：
```c
static const unsigned char ascii_char_table2[] =
{
    '#', '@', '&', '$', '%', '|', '!', ';', ':', '\'', '`', '.', ' '
};
```

#### 区块数据读取
----------------
可以根据指定的区块宽度和高度，通过遍历BMP图像数据就可取得必要的区块数据：
```c
    /* 图像数据处理 */
    for(h = 0; h < artH; h++)
    {
        /* 区块起始行 */
        int startY = h * pixH;
        /* 区块起始行地址 */
        unsigned char *ptemp = pdata->data + startY * pdata->widthbytes;
        for(w = 0; w < artW; w++)
        {
            /* 区块起始列 */
            int startX = w * pixW;
            /* 定位到行首 */
            unsigned char *q = ptemp;
            /* 定位到区块起始列 */
            q += startX * 3;

            /* 读取区块数据 */
            unsigned char *pb = p_block;
            for(y = 0; y < pixH; y++)
            {
                /* 区块当前行首地址 */
                q += y * pdata->widthbytes;
                /* 复制一行数据 */
                memcpy(pb, q, pixW * 3);
                /* 区块数据定位 */
                pb += (pixW * 3);
            }
            ......
        }
    }
```

#### 检索结果字符
----------------
在读到区块数据后，便可以根据指定的算法计算其特征值，然后在查找表中取得字符后拼接出ASCII结果图像：
```c
    /* 计算区块灰度平均值作为字符查找表下标 */
    double level = pf(p_block, pixW, pixH);
    int idx = (int)((tabsize - 1) * level + 0.5);
    char *p = p_art + w + h * artW;
    /* 为结果字符串数组赋值 */
    *p = artTab[idx];
```
同样，为简化代码结构，可将其封装为一个`char *Image2AsciiArt(BMP_Data *pdata, int pixW, int pixH, const unsigned char *artTab, size_t tabsize, GETBLOCKFOO pf)`函数。

## 计算区块数据特征值
----------------
在读到区块数据后，便可以根据指定的算法计算其特征值，比如中以使用简单的平均值算法得到区块特征值：
```c
    for(i = 0; i < size; i++)
    {
        /* 计算一个像素的归一化灰度值(0.0-1.0)，注意颜色排列为：B、G、R */
        pixlevel = 0.11 * p[2] / 255 + 0.6 * p[1] / 255 + 0.29 * p[0] / 255;
        sum += pixlevel;
        p += 3;
    }

    return sum / size;
```
同样，为简化代码结构，可将其封装为一个`double GetAverage(unsigned char *pdata, int pixW, int pixH)`函数。

另外，也可以采用其它的类似取中值、LOG算子等算法实现该特征值的计算，如：
```c
/* 图像区块值计算方式 */
/* 取区块数据的中值 */
double GetMedian(unsigned char *pdata, int pixW, int pixH);
/* 对区块数据用LOG算子进行处理 */
double GetLoG(unsigned char *pdata, int pixW, int pixH);
```

## 命令行参数设置
----------------
由于需要在执行程序时，输入图像文件名称、区块宽度、区块高度和区块特征值算法等4个参数，为此需要设计带命令行参数的main函数，如：
```c
/* 测试 */
int main(int argc, char *argv[])
{
    /* 命令行参数不符合要求 */
    if(argc < 4 || argc > 5)
    {
        /* 命令行参数不符合要求  */
        /* Image2AsciiArt <imgfilename> <w> <h> [FooNum] */
        return 0;
    }

    ......

    return 0;
}
```

其实现细节请参阅`main.c`。由于需要考虑的细节内容比较多，因此程序代码较为复杂，请大家仔细阅读分析。

## 结束语
----------------
由以上分析可以看出，对于一个实际问题，首先需要经过仔细分析，确定如何保存和管理数据，设计合适的数据类型（数据结构），然后才能设计出合理可靠的处理代码。并且在代码设计过程中，需要细心处理类似字节补齐等问题。

由于个人水平有限，文中难免有不妥之处，甚至会存在错误，也希望各位读者能不吝赐教。
