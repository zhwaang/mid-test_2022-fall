# 读取文本文件中指定的行
----------------
处理文件中指定行的数据是一种常见的操作，为此，请编译如下函数以实现读取文本文件中指定的行：
```c
    char* fget_nth_line(char *str, int lineno, FILE *fp);
```
其中，str是保存读取行数据的字符串缓冲区(字符数组)指针，lineno是指定的行编号(**从1开始计数**)，fp是文件指针。如果读取成功，则返回字符串缓冲区(字符数组)首地址。如果读取失败，则返回NULL。

分析该题目的题意可以看出，主要需要完成的工作有：
1. 复位文件指针到文件头。
2. 跳过前面不需要读取的`lineno - 1`行。
3. 读取当前行。
4. 处理异常，返回NULL。

## fgets函数整行读取计数法
----------------
可以使用fgets函数逐行从文本文件中读入一行字符，同时，对读入的行行数进行计数，如果与指定的行号相等，则返回读入的结果，如：
```c
    /* 读入指定的行 */
    cur_idx = 0;
    while(fgets(str, MAXLEN, fp))
    {
        cur_idx++;

        if(cur_idx == lineno)
        {
            /* 删除fgets添加的'\n' */
            str[strcspn(str, "\n")] = '\0';

            return str;
        }
        str[0] = '\0';
    }
```
在此，直接使用了fgets的返回值作为循环结束条件，同时，在读取下一行之前，建议将字符缓冲区清空(`str[0] = '\0'`)。另外，由于fgets函数会在行尾添加'\n'，在返回之间，可以将其置为'\0'。

由于无法确认传入的文件指针fp指向了文件头，因此，**需要使用`rewind(fp);`对文件指针进行复位操作**。

其实现细节请参阅`01-fgets-ret/main.c`。该方法逻辑简单，结构清晰，但要注意返回NULL的处理。

## 行数递减fgets函数整行读取法
----------------
可以使用指定的行数作为循环上限，逐次递减用fgets函数逐行从文本文件中读入一行字符以跳过不需要的行，最后读入的一行数据就是需要读入的行，如：
```c
    /* 读入指定的行 */
    while(lineno--)
    {
        str[0] = '\0';
        if(feof(fp) || (fgets(str, MAXLEN, fp) == NULL))
        {
            return NULL;
        }
    }
    /* 删除fgets添加的'\n' */
    str[strcspn(str, "\n")] = '\0';
```
同样，在读取下一行之前，建议将字符缓冲区清空(`str[0] = '\0'`)。另外，由于fgets函数会在行尾添加'\n'，在返回之间，可以将其置为'\0'。

由于无法确认传入的文件指针fp指向了文件头，因此，**需要使用`rewind(fp);`对文件指针进行复位操作**。

其实现细节请参阅`02-reduce-while-fgets/main.c`。该方法逻辑简单，结构清晰，但要注意返回NULL的处理。

## 行数递减扫描列表整行读取法
----------------
可以使用指定的行数作为循环上限，通过扫描列表逐行从文本文件中跳过不需要的行，最后读入的一行数据就是需要读入的行，如：
```c
    /* 读入指定的行 */
    while(lineno--)
    {
        if(fgetc(fp) == EOF)
        {
            return NULL;
        }

        /* 退回一个字节 */
        fseek(fp, -1L, SEEK_CUR);
        str[0] = '\0';
        fscanf(fp, "%[^\n]", str);
        /* 跳过'\n' */
        fscanf(fp, "%*c");
    }
```
同样，在读取下一行之前，建议将字符缓冲区清空(`str[0] = '\0'`)。另外，由于需要读入一个字符来判断是否文件结束标志(EOF)，因此，在读入下一行数据前，需要退回一个字符，这可以用`fseek(fp, -1L, SEEK_CUR);`实现。

由于无法确认传入的文件指针fp指向了文件头，因此，**需要使用`rewind(fp);`对文件指针进行复位操作**。

其实现细节请参阅`03-reduce-while-scanlist/main.c`。该方法逻辑简单，结构清晰，但要注意返回NULL的处理。

## 逐字符读取逐行递减整行读取法
----------------
可以逐字符读取整行，然后跳过不需要的行，再逐字符读取一行数据作为需要读入的行，如：
```c
    /* 跳过前面的lineno - 1行 */
    while(lineno > 1)
    {
        ch = fgetc(fp);

        if(ch == '\n')
        {
            lineno--;
        }

        /* 到达指定行 */
        if(lineno == 1)
        {
            break;
        }

        if(ch == EOF)
        {
            return NULL;
        }
    }

    /* 读入指针的行 */
    i = 0;
    while((ch = fgetc(fp)) != EOF && ch != '\n' && i < MAXLEN)
    {
        str[i++]= ch;
    }
    /* 终止符 */
    str[i] = '\0';
```

由于无法确认传入的文件指针fp指向了文件头，因此，**需要使用`rewind(fp);`对文件指针进行复位操作**。

其实现细节请参阅`04-freadline/main.c`。该方法逻辑简单，结构清晰，但要注意返回NULL的处理。

## 逐行递减用扫描列表整行读取法
----------------
可以使用指定的行数作为循环上限，通过扫描列表逐行从文本文件中跳过不需要的行，最后读入的一行数据就是需要读入的行，如：
```c
    /* 读入指定的行 */
    while(lineno--)
    {
        if(ftell(fp) == fsize)
        {
            return NULL;
        }

        str[0] = '\0';
        fscanf(fp, "%[^\n]", str);
        /* 跳过'\n' */
        fseek(fp, 1L, SEEK_CUR);
    }
```
同样，在读取下一行之前，建议将字符缓冲区清空(`str[0] = '\0'`)。另外，由于需要读入一个字符来判断是否文件结束标志(EOF)，因此，通过当前文件位置是否在文件尾判断是否提前结束循环。

由于无法确认传入的文件指针fp指向了文件头，因此，**需要使用`rewind(fp);`对文件指针进行复位操作**。

其实现细节请参阅`05-reduce-while-scanlist-fseek/main.c`。该方法逻辑简单，结构清晰，但要注意返回NULL的处理。

## 结束语
----------------
由以上分析可以看出，同一个问题可以有多种解决方案，不同方案有不同的优缺点。虽然就一个题目来讲，不需要考虑使用过多的解决方法，以最简单快捷便于理解和编写的方法通过测试即可，但显然多思考、多训练对于C语言程序设计的学习和理解是有极大帮助的。

由于个人水平有限，文中难免有不妥之处，甚至会存在错误，也希望各位读者能不吝赐教。
