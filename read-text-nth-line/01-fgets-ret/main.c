/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：读取文本文件指定行文本
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月14日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 256

/* 函数原型 */
char* fget_nth_line(char *, int, FILE *);

int main()
{
    char str[MAXLEN], *p;
    const char *filename = "test.dic";
    FILE *fp;
    int i;

    fp = fopen(filename, "r");
    if(fp == NULL)
    {
        return 1;
    }

    /* 读入指定行数据 */
    for(i = 0; i < 4; i++)
    {
        /* 文件行计数从1开始 */
        p = fget_nth_line(str, i + 1, fp);
        if(p != NULL)
        {
            puts(p);
        }
    }

    fclose(fp);

    return 0;
}

/*--------------------------------------------------------------------
// 名称：char* fget_nth_line(char *str, int lineno, FILE *fp)
// 功能：读入文本文件中指定的行
// 参数：
//       [char *str] ---- 结果字符串指针(需要有空间可写)
//       [int lineno] --- 行号，从1开始计数
//       [FILE *fp] ----- 文件指针
// 返回：[char*] --- 读入的字符串
// 作者：耿楠
// 日期：2022年12月14日
---------------------------------------------------------------------*/
char* fget_nth_line(char *str, int lineno, FILE *fp)
{
    int cur_idx; /* 当前行 */

    if(str == NULL || fp == NULL) return NULL;

    /* 文件指针复位 */
    rewind(fp);

    /* 读入指定的行 */
    cur_idx = 0;
    while(fgets(str, MAXLEN, fp))
    {        
        cur_idx++;

        if(cur_idx == lineno)
        {
            /* 删除fgets添加的'\n' */
            str[strcspn(str, "\n")] = '\0';

            return str;
        }
        str[0] = '\0';
    }

    return NULL;
}
