/*--------------------------------------------------------------------------------
* Copyright (c) 2022,西北农林科技大学信息学院计算机科学系
* All rights reserved.
*
* 文件名称：main.c
* 文件标识：见配置管理计划书
* 摘要：读取文本文件指定行文本
*
* 当前版本：1.0
* 作者：耿楠
* 完成日期：2022年12月14日
*
* 取代版本：无
* 原作者：
* 完成日期：
--------------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 256

/* 函数原型 */
char* fget_nth_line(char *, int, FILE *);

int main()
{
    char str[MAXLEN], *p;
    const char *filename = "test.dic";
    FILE *fp;
    int i;

    fp = fopen(filename, "r");
    if(fp == NULL)
    {
        return 1;
    }

    /* 读入指定行数据 */
    for(i = 0; i < 4; i++)
    {
        /* 文件行计数从1开始 */
        p = fget_nth_line(str, i + 1, fp);
        if(p != NULL)
        {
            puts(p);
        }
    }

    fclose(fp);

    return 0;
}

/*--------------------------------------------------------------------
// 名称：char* fget_nth_line(char *str, int lineno, FILE *fp)
// 功能：读入文本文件中指定的行
// 参数：
//       [char *str] ---- 结果字符串指针(需要有空间可写)
//       [int lineno] --- 行号，从1开始计数
//       [FILE *fp] ----- 文件指针
// 返回：[char*] --- 读入的字符串
// 作者：耿楠
// 日期：2022年12月14日
---------------------------------------------------------------------*/
char* fget_nth_line(char *str, int lineno, FILE *fp)
{
    /* 入口参数判断 */
    if(str == NULL || fp == NULL) return NULL;

    int i;
    int ch;

    /* 文件指针复位 */
    rewind(fp);

    /* 跳过前面的lineno - 1行 */
    while(lineno > 1)
    {
        ch = fgetc(fp);

        if(ch == '\n')
        {
            lineno--;
        }

        /* 到达指定行 */
        if(lineno == 1)
        {
            break;
        }

        if(ch == EOF)
        {
            return NULL;
        }
    }

    /* 读入指针的行 */
    i = 0;
    while((ch = fgetc(fp)) != EOF && ch != '\n' && i < MAXLEN)
    {
        str[i++]= ch;
    }
    /* 终止符 */
    str[i] = '\0';

    return str;
}
